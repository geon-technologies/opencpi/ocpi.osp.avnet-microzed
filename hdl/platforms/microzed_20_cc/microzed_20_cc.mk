#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Kolton Fodel
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Assigns the particular Vivado Board Part number and the RCC 
#              platform that it can target
#==============================================================================

HdlPart_microzed_20_cc=xc7z020-1-clg400
HdlRccPlatform_microzed_20_cc=xilinx19_2_aarch32
HdlAllRccPlatforms_microzed_20_cc=xilinx19_2_aarch32