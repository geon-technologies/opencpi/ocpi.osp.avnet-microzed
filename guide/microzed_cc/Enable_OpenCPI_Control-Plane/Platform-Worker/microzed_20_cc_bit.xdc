#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Kolton Fodel
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: File for customizing Vivado project-level properties. When this
#              file is "empty", the defaults are used.
#==============================================================================
