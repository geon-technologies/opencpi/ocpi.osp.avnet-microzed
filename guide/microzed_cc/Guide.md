# Guide for developing an OpenCPI Board Support Package (OSP) - Case Study MicroZed 7020 and FMC Carrier Card 

## Table Of Contents
1. [Introduction](#introduction)
    1. [Framework Software](#framework-software)
    2. [Embedded SW Platform](#embedded-sw-platform)
    3. [Prerequisites](#prerequisites)
    4. [Preview of the concluding directory tree](#preview-of-the-concluding-directory-tree)
    5. [Review of the vendor's Reference Design Package](#review-of-the-vendor's-reference-design-package)
2. [Design Staging](#design-staging)
    1. [Identify "minimal" configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)
    2. ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf))
3. [OpenCPI Staging](#opencpi-staging)
    1. [Install the OpenCPI framework](#install-the-opencpi-framework)
    2. [Configure a host terminal for OpenCPI development](#configure-a-host-terminal-for-opencpi-development)
    3. [Take inventory of the target board](#take-inventory-of-the-target-board)
    4. [Update the OpenCPI Framework (No action required just a review)](#update-the-opencpi-framework-(no-action-required-just-a-review))
        1. [Enabling Development for New GPP Platforms (Not Applicable)](#enabling-development-for-new-gpp-platforms-(not-applicable))
        2. [Installing the Tool Chain (Not Applicable)](#installing-the-tool-chain-(not-applicable))
        3. [Integrating the Tool Chain into the OpenCPI HDL Build Process (Not Applicable)](#integrating-the-tool-chain-into-the-opencpi-hdl-build-process-(not-applicable))
    5. [Bug fixes to the framework](#bug-fixes-to-the-framework)
    6. [Benchmark testing the zed OSP](#benchmark-testing-the-opencpi-zed-osp)
    7. [Modifications to the Install and Deploy scripts](#modifications-to-the-install-and-deploy-scripts)
    8. [Install and Deploy the platforms: zed, xilinx13_3](#install-and-deploy-platforms:-zed,-xilinx13_3)
    9. [Create an OpenCPI project for the OSP](#create-an-opencpi-project-for-the-osp)
4. [Enable OpenCPI HDL Control Plane](#enable-opencpi-hdl-control-plane)
    1. [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)
    2. [PetaLinux workspace for Control Plane](#petalinux-workspace-for-control-plane)
    3. [Configure HDL Primitive for Control Plane](#configure-hdl-primitive-for-control-plane)
    4. [Configure HDL Platform Worker for Control Plane](#configure-hdl-platform-worker-for-control-plane)
    5. [Build the HDL Platform with Control Plane enabled](#build-the-hdl-platform-with-control-plane-enabled)
    6. [HDL Control Plane verification: read OpenCPI "Magic Word"](#hdl-control-plane-verification-read-opencpi-magic-word)
    7. [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled)
    8. [HDL Control Plane verification: test application pattern_capture](#hdl-control-plane-verification:-test-application-pattern_capture)
5. [Enable OpenCPI HDL Data Plane](#enable-opencpi-hdl-data-plane)
    1. [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)
    2. [PetaLinux workspace for Data Plane](#petalinux-workspace-for-data-plane)
    3. [Configure HDL Primitive for Data Plane](#configure-hdl-primitive-for-data-plane)
    4. [Configure HDL Platform Worker for Data Plane](#configure-hdl-platform-worker-for-data-plane)
    5. [Build the HDL Platform with Data Plane enabled](#build-the-hdl-platform-with-data-plane-enabled)
    6. [Undo Edits made to validate HDL Control Plane](#undo-edits-made-to-validate-hdl-control-plane)
    7. [Install and Deploy the HDL Platform with Data Plane enabled](#install-and-deploy-the-hdl-platform-with-data-plane-enabled)
    8. [HDL Data Plane verification: testbias application](#hdl-data-plane-verification:-testbias-application)
    9. [HDL Data Plane verification: test FSK application filerw mode](#hdl-data-plane-verification:-test-fsk-application-filerw-mode)
6. [OpenCPI Component Unit Testing](#opencpi-component-unit-testing)
    1. [Server Mode setup](#server-mode-setup)
    2. [Build and Run](#build-and-run)
7. [Enable OpenCPI Cards & Slots](#enable-opencpi-cards-&-slots)
    1. [Configure HDL Platform Worker with Slot](#configure-hdl-platform-worker-with-slot)
    2. [Building the HDL Platform Worker with Slot enabled](#building-the-hdl-platform-worker-with-slot-enabled)
    3. [Install and Deploy the Slot configuration](#install-and-deploy-the-slot-configuration)
    4. [Cards & Slots verification: FSK modem on the fmcomms2](#cards-&-slots-verification:-fsk-modem-on-the-fmcomms2)
        1. [Building the FSK_modem HDL Assembly](#building-the-fsk_modem-hdl-assembly)
        2. [Running the FSK_modem_app HDL Assembly](#running-the-fsk_modem_app-hdl-assembly)
    5. [Build and run all Component Unit Tests for Cards & Slots](#build-and-run-all-component-unit-tests-for-cards-&-slots)
8. [APPENDIX](#appendix)
    1. [Petalinux Build](#petalinux-build)
        1. [Static IP-Address Setup](#static-ip-address-setup)
    2. [FSK filerw for 7010](#fsk-filerw-for-7010)  
        1. [Building the fsk_filerw_10 Assembly](#building-the-fsk_filerw_10-assembly)   
        2. [Building the FSK_10 filerw Application](#building-the-fsk_10-filerw-application)  
        3. [Run the FSK_10 filerw Application on the microzed_10_cc](#run-the-fsk_10-filerw-application-on-the-microzed_10_cc)  
    3. [FSK Modem for 7010](#fsk-modem-for-7010)  
        1. [Building the fsk_modem_10 Assembly](#building-the-fsk_modem_10-assembly)
        2. [Creating the drc_7010 RCC Worker](#creating-the-drc_7010-rcc-worker)
        3. [Running the FSK_modem_10 app](#running-the-fsk_modem_10-app)
    4. [Reference Design](#reference-design)
    5. [Network-Mode setup](#network-mode-setup)
    6. [Test result tables](#test-result-tables)
        1. [Component Unit Test result table](#component-unit-test-result-table)
        2. [Verification test result table](#verification-test-result-table)
    7. [FMC Daughter Board Connection Table](#fmc-daughter-board-connection-table)  
<br> 

---------------------------------------------------------------------------------------------------
<a name="introduction"></a>

## Introduction
---------------------------------------------------------------------------------------------------

This document serves as a guide for developing an OpenCPI Board Support Package (OSP) for boards based on a Xilinx Zynq-7000 System-on-a-Chip (SoC). Specifically, it is a case study for developing an OSP for the MicroZed 7020 and its Carrier Card (MZCC).  Only the "base" configuration of the platform is detailed in this guide.  A “base” configuration is one that supports the command/control, and high throughput data transfer, more commonly referred to as Control Plane and Data Plane, respectively, but does not include device workers. The MicroZed can be used both as a standalone hardware accelerator and installed on a carrier card. When used as a standalone board, power is provided through the Micro USB connected J2. When installed on the FMC Carrier Card, the carrier card supplies power/gnd to the MicroZed via the JX1 and JX2 connectors. For this OSP, only the FMC-LPC connector slot will be defined to allow support for daughtercards with support for FMC-LPC connectivity. Features that are not included in a “base” configuration are those that are not critical to support the functionality of the Control Plane and Data Plane.  In summary, tor the MicroZed and its Carrier Card (CC), the "base" configuration BSP is one with the following supported features:

1. Control Plane
2. Data Plane
3. Slot definition for the FMC-LPC

While the MicroZed's Carrier-Card does have several PMODs, pushbuttons and other peripheral interfaces, NO additional Device Worker support are to be implemented.

Since the Xilinx Zynq-7000 SoC on the MicroZed is of the same device family that is on the Zedboard (a.k.a "zed"), the Zedboard's OSP is referenced heavily in this guide. In fact, the Zedboard's OSP will be examined in the following areas and will serve as the benchmark for defining the requirements for the MicroZed's OSP:

1. What are the output artifacts of performing an OpenCPI Install and Deploy of the zed (and the xilinx13_3)
2. Review of the HDL primitive and HDL platform source code for implementing the zed
3. Control Plane (CP) ONLY validation - run CP reference application "pattern_capture" on the zed HDL platform
4. Data Plane (DP) validation of the following on the zed HDL platform
    1. DP reference application, i.e. "testbias"
    2. Run FSK "filerw"
    3. Run all Component Unit Tests to establish their Pass/Fail benchmarks
8. Slot (FMC) support - run reference applications fsk_modem_app on the zed with an fmcomms2 daughtercard

<a name="framework-software"></a>

### Framework Software
Because the framework already supports the Xilinx Zynq-7000 processor in support of the Zedboard, those portions of the framework's software which perform the command/control and data transport will simply be reused in support of the MicroZed OSPs. As such, this guide will not perform a deep dive into those software sections, but will attempt to make note of the relevant software modules when appropriate.

<a name="embedded-sw-software"></a>

### Embedded SW Platform
In addition to enabling the MicroZed's FPGA portion of the SoC, an RCC platform will also be targeted for running artifacts on the SoC's ARM, i.e. the processing system (PS). This requires setup of a cross-compiler for building the run-time utilities of OpenCPI.

By reviewing the table of contents, the developer can gain a high-level understanding of the implementation flow that is performed in this guide.  Note that there are various points throughout the guide which require repeating/reusing of steps in previous sections.

**CRITICAL NOTES:**
- **Version v2.1.0 of the OpenCPI FOSS framework is used for this development effort.**  
- **The zed OSP is used as the reference Zynq-7000 platform for which the microzed_20_cc OSP is based.** 
- **Bug fixes to the FOSS are REQUIRED and detailed in the guide.**
- **For this guide, the default home or working directory is "/home/user".**

**CODE BLOCKS:**  
**Throughout this document there will be references to code that can be found within the repository of the OSP. The directory structure is as follows /home/user/opencpi/projects/osp/ocpi.osp.avnet/guide/microzed_cc/\<section>. When necessary, the specific section and file will be outlined.**  
<br>

<a name="deliverables"></a>

### **Deliverables**
---------------------------------------------------------------------------------------------------
1. An OpenCPI project named, "ocpi.osp.avnet"  
2. Two OpenCPI Board Support Packages (OSP) with the software platform dependency of "xilinx19_2_aarch32"
    1. microzed_20_cc   
    2. microzed_10_cc  
3. All OSPs support the HDL Control Plane, Data Plane, and the FMC-LPC slot of the MZCC
4. Documentation  
    1. OSP Getting Started Guide  
    2. Guide for developing an OSP for a Zynq-7000 based platforms - Case Study: MicroZed+CC (this guide)  
<br>

<a name="prerequisites"></a>

### **Prerequisites**
---------------------------------------------------------------------------------------------------
1. *Knowledge*
    1. A working knowledge of the OpenCPI framework
    2. The developer should be familiar with the various OpenCPI documents, but the following are specifically important for this guide:
        - OpenCPI Platform Development Guide: https://opencpi.gitlab.io/releases/v2.1.0/docs/OpenCPI_Platform_Development_Guide.pdf
        - Component Development Guide (Component Unit Test): https://opencpi.gitlab.io/releases/v2.1.0/docs/OpenCPI_Component_Development_Guide.pdf
        - Getting Started Guide for the Zedboard: https://opencpi.gitlab.io/releases/v2.1.0/docs/platform/ZedBoard_Getting_Started_Guide.pdf
        - Getting Started Guide for the FSK applications: https://opencpi.gitlab.io/releases/v2.1.0/docs/assets/FSK_App_Getting_Started_Guide.pdf
        - Support documentation for the fmcomms2/3 daughtercard

2. *Equipment*

    | Item                      | Description                  | Vendor  | P/N                          | Cost  | 
    |---------------------------|------------------------------|---------|------------------------------|-------|
    | MicroZed 7020             | SOM - Zynq-7000 SoC based    | Avnet   | AES-Z7MB-7Z020-SOM-G/REV-G   | 213 $ |
    | MicroZed 7010             | SOM - Zynq-7000 SoC based    | Avnet   | AES-Z7MB-7Z010-SOM-I-G/REV-G | 217 $ |
    | MicroZed FMC Carrier Card | MicroZed FMC Carrier Card    | Avnet   | AES-MBCC-FMC-G               | 199 $ |
    | fmcomms2                  | High-speed analog module     | Digikey | AD-FMCOMMS2-EBZ              | 750 $ |

3. *Tools*
    1. Xilinx tools: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2019-2.html  
        1. Download the "Unified Installer"  
            1. Vivado 2013.4 : build bitstream for xilinx13_3
            2. Vivado 2019.2 : build bitstream for xilinx19_2_aarch32/
            3. Vitis 2019.2  : cross-compile OCPI runtime utilities for embedded operating system  
        2. PetaLinux 2019: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2019-2.html  
            1. For creating embedded Linux image for SD card  
    2. OpenCPI FOSS framework, v2.1.0: https://gitlab.com/opencpi/opencpi.git  
<br>

<a name="preview-of-the-concluding-directory-tree"></a>

### **Preview of the concluding directory tree**
---------------------------------------------------------------------------------------------------
This guide requires the creation of many directories. A summary of those directories is provided below:

**OpenCPI directory**
~~~
/home/user/opencpi 
~~~

**MicroZed project repo directory**
~~~
/home/user/opencpi/projects/osps/ocpi.osp.avnet
~~~

**Reference design directory**
~~~
/home/user/vendor_reference_design_package
~~~

**ZynqReleases directory**
~~~
/home/user/2019.2-microzed_20_cc-release  
~~~

**Petalinux directories**
~~~
/home/user/plx_7020_vendor
/home/user/plx_7020_minimal
/home/user/plx_7020_cp
/home/user/plx_7020_dp
~~~

**Processing-System vivado project directories**
~~~
/home/user/ps7_microzed_7020_preset
/home/user/ps7_microzed_7020_refdsgn (OPTIONAL)
/home/user/ps7_microzed_7020_ocpi_cp
/home/user/ps7_microzed_7020_ocpi_dp
~~~  
<br>

<a name="review-of-the-vendor's-reference-design-package"></a>

### **Review of the vendor's Reference Design Package**
---------------------------------------------------------------------------------------------------
GOALS:
- Allow the user to become familiar with the target board and its reference design package. Review its documentation and work though the reference example designs for both the processor and the FPGA. By working through the reference design, the user will become comfortable with various tools, modifying source code, understanding the build flow (Vivado, PetaLinux), creating an SD card, loading the FPGA and running an application. The [Petalinux Build](#petalinux-build)  flow steps captured during this review will be relied upon heavily throughout much of the remainder of this guide and its lesson-learned will be used when integrating with OpenCPI.
- (RECOMMENDED) Verify that the reference design can be modified and rebuilt so that the 4 PL LEDs reflect a unique pattern, referred to as a "unique identifier". Steps are provided to implement simple modifications to the top-level HDL and constraints file to set the LEDS to a unique pattern. Then build the bitstream, create an SD image via PetaLinux, install on the MZCC and observe the the "unique identifier" pattern on the 4 PL LEDs.

Download reference design package:
1. Create a directory named "/home/user/vendor_reference_design_package"
2. To download the reference design, navigate to https://www.element14.com/community/docs/DOC-95638/l/microzed. Locate the 'Reference Designs' tab, navigate to the 'PetaLinux Board Support Packages' and download:
    1. PetaLinux 2019.2 Compressed BSP, z70**
        1. Once the reference design is downloaded, move it into the "vendor_reference_design_package" directory.
    2. At the time of creating this document the MD5SUM of the zip files are outlined below:
        1. mz7020_fmccc_2019_2.zip 2aef9329e844ea89f1aae83e6a1015f3 
        1. mz7010_fmccc_2019_2.zip b5337763c80069950b6a91de69c210f1
    3. Unzip and untar the file:  
        $ cd /home/user/Downloads
        $ unzip mz7020_fmccc_2019_2.zip  
        $ tar xf mz7020_fmccc_2019_2.bsp
    4. The reference designs in the links do not contain FMC constraint files since the MicroZed can be used as a standalone board. To get the constraint files for the FMC Carrier Card, you must navigate to https://www.element14.com/community/docs/DOC-91227/l/fmc-carrier-card-for-microzed-evaluation-kit?ICID=microzed-accessories-widget. Under the "Downloads" section on the right side of the page, under the "Board Support Package" section, click "Z7010 or Z7020 MicroZed with MBCC-FMC-PCB-B_v2 (zip)" to download the constraints file. 
    5. Unzip the constraints file and move it into /home/user/vendor_reference_design_package:  
        $ cd /home/user/Downloads
        $ unzip  AES-MBCC-FMC-G-PCB-B_v2.zip
        $ cp Z7010 or Z7020 MicroZed with MBCC-FMC-PCB-B_v2.xdc /home/user/vendor_reference_design_package
    6. At the time of creating this document the MD5SUM of the constraint files are outlined below:
        1. "Z7010 or Z7020 MicroZed with MBCC-FMC-PCB-B_v2.xdc" b45885817985efdf8a43249571d75d89 
3. Review the documentation in the reference design package.
4. Per the documentation, setup the development host with appropriate tools.
    - Make note of any special setup requirements.
5. Build the reference design(s). This includes loading the FPGA and running a simple application to toggle available LEDs. Explore all that has been provided. 
    1. Make note of any special build or run time steps, which may be reused in the OpenCPI development flow:
        1. Build the bitstream
        2. [Petalinux Build](#petalinux-build) flow (captured in the the APPENDIX)
        3. Setting up the SD card (captured in a subsequent section)
        4. Load the FPGA
        5. Run the Application(s)
6. Once you have become familiar with the out-of-the-box reference design, convince yourself that you can edit the design with a "unique identifier", such as, a unique pattern on the Programmable Logic LEDs that are present on the MZCC.  
    **CODE BLOCK**  
    **The codeblocks of the MZ7020_FMCCC_wrapper.vhd and the MZ7020_FMCCC_user_io.xdc can be found in the following directory of the ocpi.osp.avnet repository:**  
    - **ocpi.osp.avnet/guide/microzed_cc/Introduction/MZ7020_FMCCC_wrapper.vhd**
    - **ocpi.osp.avnet/guide/microzed_cc/Introduction/MZ7020_FMCCC_user_io.xdc** 
    Open the Microzed 7020 reference design
    1. cd /home/user/vendor_reference_design_package/mz7020_fmccc_2019_2/hardware/MZ7020_FMCCC_2019_2/
    2. source /opt/Xilinx/Vivado/2019.2/settings64.sh
    3. Vivado MZ7020_FMCCC.xpr &
    Start by modifying the MZ7020_FMCCC_wrapper.vhd file and add the following lines:
    1. At the bottom of the "entity MZ7020_FMCCC_wrapper is", add the following port declaration :  
        leds : out STD_LOGIC_VECTOR( 3 downto 0 )  
    2. After the architectural "begin", make to following assignment to set your "unique identifier"
        leds <= "0101";  
    Since there isn't an included user_io constraints file, one must be created so that the led "unique identifier" can be implemented.
    1. Navigate to the "Flow Navigator" -> "Add Sources" -> "Add or create constraints" -> "Create File" ->  name the file "MZ7020_FMCC_user_io"
    2. Open the constraint file MZ7020_FMCCC_user_io.xdc and add the following lines for LED 0-3:
    ~~~
    set_property PACKAGE_PIN R19 [get_ports [list leds[0]]];  # "PL_LED1"
    set_property PACKAGE_PIN V13 [get_ports [list leds[1]]];  # "PL_LED2"
    set_property PACKAGE_PIN K16 [get_ports [list leds[2]]];  # "PL_LED3"
    set_property PACKAGE_PIN M15 [get_ports [list leds[3]]];  # "PL_LED4"
    
    set_property IOSTANDARD LVCMOS33 [get_ports leds[0]];
    set_property IOSTANDARD LVCMOS33 [get_ports leds[1]];
    set_property IOSTANDARD LVCMOS33 [get_ports leds[2]];
    set_property IOSTANDARD LVCMOS33 [get_ports leds[3]];
    ~~~

<a name="petalinux-workspace-for-vendor-reference-design"></a>

### PetaLinux workspace for Vendor Reference Design
---------------------------------------------------------------------------------------------------
1. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly.  
2. Create the PetaLinux workspace for the development of Control Plane ONLY SD card image
    $ /home/user/plx_7020_vendor 
    **CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, you WILL include the --fpga argument as this section will produce a bitstream.**  
<br>

---------------------------------------------------------------------------------------------------
<a name="design-staging"></a>

## Design Staging
---------------------------------------------------------------------------------------------------

<a name="identify-minimal-configuration-of-ps-core-ip"></a>

### Identify minimal configuration of PS core IP
---------------------------------------------------------------------------------------------------
BACKGROUND:

A comparison of the Zedboard and MicroZed reference designs revealed that the configurations of the PS core IPs were different in several ways, with the most notable being:

1. DDR Memory configuration: zed(128MB), microzed(256MB)

Given these differences with the DDR on the two platforms, the configuration of the PS core IP for the Zedboard would not properly function on the MicroZed.  Therefore, a PS core IP configuration must be identified for the MicroZed.

GOALS:
- Determine the minimal configuration of the PS core IP that will allow the embedded Linux OS to boot and operate normally on the target platform. This step will ensure that the PS core IP is properly configured for communicating with its attached memory.  Once the minimal configuration is verfied, then the PS core IP can be modified to enable features for supporting OpenCPI.  
- Two approaches are detailed for ascertaining the minimal configuration of the PS core IP:  
    1. ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) - approach does not include a constraint file.   
    **CRITICAL NOTE: The ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) is the method used and outlined thoughout this guide.**
    2. [Reference Design](#reference-design) - has a constraint file available that has been provided by Avnet. This approach provides a known "good" constraints file and gives the developer the added ability to quickly implement a 'unique identifier' (LED) for confirming that the FPGA has also been programmed with a custom bitstream.  
    **CRITICAL NOTE: The [Reference Design](#reference-design) method is located in the APPENDIX for reference and has been confirmed to produce the required artifacts necessary for developing an OSP.**
- Either approach:
    1.  Can be used to generate output products which can be used by the [Petalinux Build](#petalinux-build) flow to build an SD card for successfully booting the embedded OS on the platform
    2. Will result in a Vivado project that provides a starting point for enabling the OpenCPI BSP
    3. Will create a primitive of the Processing System (PS) core IP that is unique to the MicroZed. This Processing System (PS) core IP can be wrapped in an OpenCPI primitive and instanced in the OpenCPI platform worker.   
<br>

<a name="preset-board-design-files-(bdf)"></a>

### 'Preset' Board Design Files (BDF)
---------------------------------------------------------------------------------------------------
GOALS:
- Use the Board Design File (BDF) for targeting the MicroZed board, which provides the ability to "Preset" the configuration of the PS core IP that is unique to the target board. The BDF defines things that are unique to the target board, such as the configuration of the DDR.
- This section details creating a new Vivado project based on the Target Board "MicroZed 7020 SOM + FMC Carrier" and block design, where a PS core IP (Zynq-7000) is instanced and "Preset", i.e. configured, to support the MicroZed 7020 SOM that is installed on the FMC Carrier Card.
- The output of this section defines the "minimal" configuration that is Generated, HDL Wrapped and Exported for use in the Petalinux project that is used to create an SD-Card linux image which sucessfully boots on the target board.
- Once these steps are confirmed, then signals and ports (clock, reset, master (CP), slave (DP)) that are required for OpenCPI can be included, but this is detailed in a later step.

**NOTE:** All default settings of the PS core IP as a result of applying the 'Preset', are accepted, unless specified in the steps below.

1. Download the appropriate *.bdf file from https://github.com/Avnet/bdf)
2. Install the *.bdf into /.../Xilinx/Vivado/version/data/boards/board_files
3. From the /home/user directory, launch Vivado
4. Create a new Vivado project, named "ps7_microzed_7020_preset"
    - Ensure that the "Create project subdirectory" box is checked, and confirm the "Project Loacation" is in the desired directory (/home/user/) level prior to clicking "OK". This will provide a seperate project for configuring the PS core IP to support the 'preset' only.  
    - **CRITICAL NOTE:  A similar step will be performed when developing support for the CP and the DP. This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
5. Proceed to "Default Part" and select the target a "Boards" tab, "MicroZed 7020 Board"
6. Create Block Design named "preset_config_wrapper"  
**CRITICAL NOTE: Important step, as this naming convention will be used moving forward through this document**
7. Add IP: "ZYNQ7 Processing System"
8. "Run Block Automation", and accept "Apply Board Preset", and select OK.  
(https://forums.xilinx.com/t5/Processor-System-Design-and-AXI/Zynq7-PS-presets-issue/td-p/1090179)
9. Double click on the Zynq processor block to open the customization GUI. Navigate to the PS-PL Configuration Page and enable the M AXI GP0 Interface within the AXI Non Secure Enablement -> GP Master AXI Interface menu. 
10. Make a connection from M_AXI_GP0_ACLK to FCLK_CLK0
11. The design should look as follows:   
![Preset Design BD](./images/preset_design_BD.png)  
12. Perform "Generate Block Design".
13. Click OK, when the Critical Messages pop-up window appears.
14. In the "Sources" tab and "Hierarchy" view, right mouse click the "preset_config_wrapper" design and click "Create HDL Wrapper"
15. After the Generate Block Design is complete, export the hardware by selecting:
    1. File → Export → Export Hardware
    2. Ensure "Include Bitstream" is not checked
    3. Confirmed the "Export to" path
    4. Click Ok
16. Excute the steps detailed in the [Petalinux Build](#petalinux-build) section (example provided below) to update/create the SD card and confirm that the Embedded Linux will boot properly when installed on the MicroZed.  
**CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section does not produce a bitstream.**  
<br>
    
    ~~~
    $ cd /home/user

    $ petalinux-create -t project --template zynq --name plx_7020_minimal
    INFO: Create project: plx_7020_minimal
    INFO: New project successfully created in /home/user/

    $ cd plx_7020_minimal
     
    $ petalinux-config --get-hw-description=../ps7_microzed_7020_preset
    INFO: Getting hardware description...
    INFO: Rename preset_config_wrapper_wrapper.xsa to system.xsa
    [INFO] generating Kconfig for project
    [INFO] menuconfig project
    configuration written to /home/user/plx_7020_minimal/project-spec/configs/config
     
    *** End of the configuration.
    *** Execute 'make' to start the build or try 'make help'.
     
    [INFO] sourcing bitbake
    [INFO] generating plnxtool conf
    [INFO] generating meta-plnx-generated layer
    [INFO] generating user layers
    [INFO] generating workspace directory
    [INFO] generating machine configuration
    [INFO] generating bbappends for project . This may take time !
    [INFO] generating u-boot configuration files
    [INFO] generating kernel configuration files
    [INFO] generating kconfig for Rootfs
    [INFO] silentconfig rootfs
    [INFO] generating petalinux-user-image.bb
     
    $ petalinux-build
    ........
     
    $ cd images/linux/
     
    $ petalinux-package --boot --fsbl --u-boot --force
    INFO: File in BOOT BIN: "/home/user/plx_7020_minimal/images/linux/zynq_fsbl.elf"
    INFO: File in BOOT BIN: "/home/user/plx_7020_minimal/images/linux/u-boot.elf"
    INFO: Generating Zynq binary package BOOT.BIN...
     
     
    ****** Xilinx Bootgen v2019.2
      **** Build date : Oct 23 2019-22:59:42
        ** Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
     
    INFO: Binary is ready.
    WARNING: Unable to access the TFTPBOOT folder /tftpboot!!!
    WARNING: Skip file copy to TFTPBOOT folder!!!
     
    $ cp BOOT.BIN image.ub /run/media/user/sd-card/
    Remove SD card from host
     
    Install SD card into MicroZed, and power on MicroZed
    From host, start a screen session
     
    $ sudo screen /dev/ttyUSB0 115200
    Zynq>
     
    (!!!THIS NEXT STEP MAY ONLY BE NECESSARY FOR THE MICROZED!!!)
    Zynq> setenv default_bootcmd run uenvboot; run cp_kernel2ram && bootm ${netstart}
    ...
     
    PetaLinux 2019.2 plx_7020_minimal /dev/ttyPS0
     
    plx_7020_minimal login: root
    Password:
    root@plx_7020_minimal:~#
     
    (SUCCESS!!!)
    ~~~
<br>

---------------------------------------------------------------------------------------------------
<a name="opencpi-staging"></a>

## OpenCPI Staging
---------------------------------------------------------------------------------------------------

<a name="install-the-opencpi-framework"></a>

### Install the OpenCPI framework
---------------------------------------------------------------------------------------------------
Download the "v2.1.0" tag of OpenCPI from the provided link, then run the installation for centos7
1. $ cd /home/user
2. $ git clone -b develop https://gitlab.com/opencpi/opencpi.git
3. $ cd opencpi
4. $ git checkout tags/v2.1.0
5. $ ./scripts/install-opencpi.sh  
<br>

<a name="configure-a-host-terminal-for-opencpi-development"></a>

### Configure a host terminal for OpenCPI development
---------------------------------------------------------------------------------------------------
As a convenience, below are the steps for configuring a Host terminal for the OpenCPI development environment
1. After the OpenCPI framework has been installed, source the OpenCPI framework setup script  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s
2. Ensure the environment is configured for the desire version of Vivado and its license file  
    $ export OCPI_XILINX_LICENSE_FILE=2100@r420  
    $ export OCPI_XILINX_VIVADO_VERSION=2019.2  
    $ env | grep OCPI
~~~
$ env | grep OCPI
OCPI_CDK_DIR=/home/user/opencpi/cdk
OCPI_PREREQUISITES_DIR=/home/user/opencpi/prerequisites
OCPI_ROOT_DIR=/home/user/opencpi
OCPI_TOOL_ARCH=x86_64
OCPI_TOOL_DIR=centos7
OCPI_TOOL_PLATFORM=centos7
OCPI_TOOL_PLATFORM_DIR=/home/user/opencpi/cdk/../project-registry/ocpi.core/exports/rcc/platforms/centos7
OCPI_TOOL_OS_VERSION=c7
OCPI_TOOL_OS=linux
OCPI_XILINX_LICENSE_FILE=2100@r420
OCPI_XILINX_VIVADO_VERSION=2019.2
~~~  
<br>

<a name="take-inventory-of-the-target-board"></a>

### Take inventory of the target board
---------------------------------------------------------------------------------------------------
BACKGROUND:  
Per the OpenCPI Platform Development Guide, the target board will be examined to identify the devices and interfaces that are required to support a functioning OpenCPI BSP. One of the main focuses of this phase is to identify the device(s) that will be implemented to support an OpenCPI “container”, which is where an application or portion of an application can be deployed. The interfaces between all devices are examined to determine if they are necessary for command/control or to transport payload data within an OpenCPI BSP. It is necessary to establish which device or interconnect to an external device serves as the “main processor”, and whose responsibility it is to perform system level functionality like, command/control, and possibly, transportation of payload data. This will aid in determining the amount of work that is necessary for developing an OpenCPI BSP to the target platform.

GOALS:
1. Identify container(s)
2. Identify interface(s)
3. Sort these items into the following categories based on their current support within the OpenCPI framework: \*\*Yes, No, Partial.\*\* For items where there is partial or no support, a research phase is conducted to ascertain the amount of development that is required to understand the level of support that is needed by the target platform. The “level of support” varies based on the configuration for a given device versus the requirements of the target platform. For example, although the digital interface of the Analog Devices transceiver may support LVDS and CMOS electrical standards, the target platform may implement support of only the LVDS standard, and therefore it may not be necessary to also enable support for CMOS. However, implementing support for CMOS standard could be added at a later time. This also highlights any additional requirement(s) for developing support for new tooling or other features that must be integrated into the OpenCPI framework to properly target the devices, such as FPGA build tools or software cross-compilers. In many cases, this initial investigation into the platform can be accomplished from vendor provided documentation, such as,  the User’s guide, board schematics and wiring diagrams. In cases where this documentation is insufficient, analysis must be performed with access to the board directly. However, it is also possible that not enough information can be gathered to successfully develop an OpenCPI BSP, but this should be determined upon the completion of the phases described above.

    |Tool/Device/Interface/Function | Description          | Framework Support (Yes/No/Partial)         |
    |-------------------------------|----------------------|--------------------------------------------|
    | T: FPGA build tools           | Xilinx Vivado 2019.2 | Yes                                        |
    | D: XC7Z020-CLG400             | Zynq-7000 SoC IC     | Yes - but not tested on this platform      |
    | D: XC7Z010-CLG400             | Zynq-7000 SoC IC     | Yes - but not tested on this platform      |
    | I: FMC-LPC                    | FMC Low Pin Count    | Yes                                        |  
<br>

<a name="update-the-opencpi-framework-(no-action-required-just-a-review)"></a>

### Update the OpenCPI Framework (No action required just a review)
---------------------------------------------------------------------------------------------------
Based on the items identified in the [Take inventory of the target board](#take-inventory-of-the-target-board) section, the below is a high-level summary of updates made to the OpenCPI framework, necessary in support of this guide. 

**NOTE:** Some updates are "Not Applicable", but listed to emphasize that they were considered. 
<br> 

<a name="enabling-development-for-new-gpp-platforms-(not-applicable)"></a>

#### Enabling Development for New GPP Platforms (Not Applicable)  
- The SW platform version (xilinx19_2_aarch32) is supported by the framework.  
<br>

<a name="installing-the-tool-chain-(not-applicable)"></a>

#### Installing the Tool Chain (Not Applicable)
- The FPGA build tools/version (Xilinx Vivado/Vitis 2019.2) is supported by the framework.  
<br>

<a name="integrating-the-tool-chain-into-the-opencpi-hdl-build-process-(not-applicable)"></a>

#### Integrating the Tool Chain into the OpenCPI HDL Build Process (Not Applicable)
- The target family and device is supported by the framework. This was determined by reviewing the contents of /home/user/opencpi/tools/include/hdl/hdl-targets.mk  
<br>  

<a name="bug-fixes-to-the-framework"></a>

### Bug fixes to the framework
---------------------------------------------------------------------------------------------------
**CRITICAL NOTES:**  
**- The following bug fix MUST be applied to the v2.1.0 framework release to address an intermittent run-time failure observed on the embedded system while running in Network and Server Modes.**  
**- The implementation of this bug fix is required PRIOR to installing (i.e. cross-compiling) the OpenCPI run-time utilities for any targeted embedded SW RCC Platform, such as, the xilinx13_3 and xilinx19_2_aarch32.**

Error:  
~~~
Assertion failed: id => m_ports.size() is false at ../gen/os/interfaces/include/OcpiOsAssert.h:118.
Stack Dump:
~~~

~~~
$ git diff runtime/dataplane/transport/src/OcpiInputBuffer.cxx
WARNING: terminal is not fully functional
-  (press RETURN) 
diff --git a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
index 3d37c6e..03276cd 100644
--- a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
+++ b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
@@ -147,7 +147,7 @@ void InputBuffer::update(bool critical)
        sizeof(BufferMetaData)*MAX_PCONTRIBS);
 
     memset(m_bmdVaddr,0,sizeof(BufferMetaData)*MAX_PCONTRIBS);
-    getPort()->getEndPoint().doneWithInput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
+    getPort()->getEndPoint().doneWithOutput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
     m_sbMd = static_cast<volatile BufferMetaData (*)[MAX_PCONTRIBS]>(m_bmdVaddr);
   }
~~~
<br>

<a name="benchmark-testing-of-opencpi-zed-osp"></a>

### Benchmark testing the OpenCPI zed OSP
---------------------------------------------------------------------------------------------------
GOALS:
- Gain a benchmark understanding of build-time and run-time utilities, as they are performed for the OpenCPI zed HDL platform and its paired SW RCC platform, xilinx13_3.
    - Build "known good" RCC and HDL platforms to aid in the development of the OSP for the MicroZed
    - Understand the impact of the contents of the zed.exports file by reviewing the outputs of install/deploy of the zed
    - Build the "canary" Control Plane (CP)-only HDL bitstreams and run its application
    - Build the "canary" Data Plane (DP) HDL bitstreams and run its application
    - Build the Component Unit Tests and run them on the zed to obtain benchmark performance metrics. These benchmark performance metrics for the zed are outlined in the [Component Unit Test result table](#component-unit-test-result-table) section.   

**CRITICAL NOTES**  
**- Deploying zed with xilinx13_4, was shown to results in either an inability for a given Zedboard to simply boot, or intermittent failures during Component Unit Testing.**  
**- However, deploying zed with xilinx13_3 did not exhibit the same performance issues and is the primary reason that this guide uses zed deployed with xilinx13_3.**   
<br>

<a name="modifications-to-the-install-and-deploy-scripts"></a>

### Modifications to the Install and Deploy scripts
---------------------------------------------------------------------------------------------------
GOALS:
- By default, the "testbias" HDL assembly (Control + Data Plane) is built as part of the installation process for a given OSP, and this bitstream is included in the deployment of said OSP. The purpose of these modifications are to replace the "testbias" HDL assembly, so that, the "canary" Control Plane-ONLY HDL assembly "pattern_capture" is built and its bitstream, and application app (pattern_capture.xml) are deployed.

1. So that the "canary" Control Plane-ONLY app (pattern_capture.xml) is included in the list of OASs which are exported to /home/user/opencpi/cdk/\<rcc-platform>/sd*/opencpi/applications. Create a symbolic link within the /home/user/opencpi/projects/assets/applications/ to pattern_capture.xml  
    $ cd /home/user/opencpi/projects/assets/applications/  
    $ ln -s pattern_capture/pattern_capture.xml pattern_capture.xml  

2. Edit the following scripts to target the assembly "pattern_capture_asm", rather than the "testbias" assemby:  
    **CRITICAL NOTES:**  
    - **It is recommended to perform a 'Find and Replace' for all occurances of "testbias" with "pattern_capture" in the files listed below. A syntax error in these files can be difficult to diagnose, therefore it is NOT recommended to simply comment out and replace the lines when making these edits.**  
    - **In a later section [Undo Edits made to validate HDL Control Plane](#undo-edits-made-to-validate-hdl-control-plane), these edits will be reverted back to their original state, so that, the "testbias" will be installed/deployed in support of enabling the Data Plane.**
   1. Edit the `export-platform-to-framework.sh` script to target pattern_capture_asm 
        - Edit /home/user/opencpi/tools/scripts/export-platform-to-framework.sh  
            ~~tbz=projects/assets/exports/artifacts/ocpi.assets.testbias_${platform}_base.hdl.0.${platform}.bitz~~  
            tbz=projects/assets/exports/artifacts/ocpi.assets.pattern_capture_asm_${platform}_base.hdl.0.${platform}.bitz  
   1. Edit the `ocpiadmin.sh` script to target pattern_capture_asm 
        - Edit /home/user/opencpi/tools/scripts/ocpiadmin.sh  
            1.  
            ~~ocpidev -d projects/assets build --hdl-platform=$platform hdl assembly testbias~~  
            ocpidev -d projects/assets build --hdl-platform=$platform hdl assembly pattern_capture_asm  
            2.  
            ~~echo "HDL platform \"$platform\" built, with one HDL assembly (testbias) built for testing."~~  
            echo "HDL platform \"$platform\" built, with one HDL assembly (pattern_capture_asm) built for testing." 
    3. Edit the `deploy-platform.sh` script to target pattern_capture_asm  
    **NOTE:** Be mindfull of the source-code spacing, the spacing below is not correct due to Markdown limitations, so do not copy and paste from here but copy and paste your source code and replace 'testbias' with 'pattern_capture_asm' in the appropriate location.  
        - Edit /home/user/opencpi/tools/scripts/deploy-platform.sh  
            ~~cp $verbose -L ../projects/assets/hdl/assemblies/testbias/container-testbias_${hdl_platform}_base/target-*/*.bitz \  
            $sd/opencpi/artifacts~~  
            cp $verbose -L ../projects/assets/hdl/assemblies/pattern_capture_asm/container-pattern_capture_asm_${hdl_platform}_base/target-*/*.bitz \  
            $sd/opencpi/artifacts  
<br>

<a name="install-and-deploy-platforms:-zed,-xilinx13_3"></a>

### Install and Deploy platforms: zed, xilinx13_3
---------------------------------------------------------------------------------------------------
GOAL:
- To install and deploy the RCC and HDL platforms used or referenced throughout the MicroZed OSP development.  
**NOTE:** The MicroZed HDL platform will not be installed here as it has not yet been built.

"Known good" RCC and HDL platforms are built to aid in the development of the OpenCPI BSP for the MicroZed. They are used to validate the installation of the framework and to ensure the run-time utilities performed on the ZedBoard as expected, prior to enabling the MicroZed.

1. CD into the OpenCPI directory  
$ cd /home/user/opencpi
2. Setup terminal for OpenCPI development  
$ source opencpi/cdk/opencpi-setup.sh -s  
3. Install: xilinx13_3 (an RCC platform)  
$ ocpiadmin install platform xilinx13_3
4. Install: zed (an HDL platform) **NOTE:** Estimated time ~6+ Hours  
$ ocpiadmin install platform zed
5. Deploy: zed with xilinx13_3  
$ ocpiadmin deploy platform xilinx13_3 zed  
<br>

<a name="create-an-opencpi-project-for-the-osp"></a>

### Create an OpenCPI project for the OSP
---------------------------------------------------------------------------------------------------
GOAL:
- To create a skeleton project directory for the MicroZed OSP and update the project-registry to include the said project.

1. Create a project, under opencpi/projects/osps  
    $ cd /home/user/opencpi/projects/osps/  
    $ ocpidev create project ocpi.osp.avnet      
    $ cd ocpi.osp.avnet
2. Edit `Project.mk`   
    ~~~
    PackageName=osp.avnet
    PackagePrefix=ocpi
    ProjectDependencies=ocpi.platform ocpi.assets
    ComponentLibraries+=misc_comps util_comps dsp_comps comms_comps
    ~~~
3. Register ocpi.osp.avnet project  
    $ ocpidev register project 
    ~~~
    $ ocpidev register project
    Successfully registered the ocpi.osp.avnet project: /home/user/opencpi/projects/osps/ocpi.osp.avnet
    Into the registry: /home/user/opencpi/project-registry
    
    make: Entering directory `/home/user/opencpi/projects/osps/ocpi.osp.avnet'
    make: Leaving directory `/home/user/opencpi/projects/osps/ocpi.osp.avnet'
    ~~~
4. Confirm that the ocpi.osp.avnet project is registered  
    $ ocpidev show registry
    ~~~
    Project registry is located at: /home/user/opencpi/project-registry
    --------------------------------------------------------------------------------------------------
    | Project Package-ID  | Path to Project                                            | Valid/Exists |
    | ------------------  | -----------------------------------------------------------| ------------ |
    | ocpi.core           | /home/user/opencpi/projects/core                           | True         |
    | ocpi.tutorial       | /home/user/opencpi/projects/tutorial                       | True         |
    | ocpi.assets_ts      | /home/user/opencpi/projects/assets_ts                      | True         |
    | ocpi.assets         | /home/user/opencpi/projects/assets                         | True         |
    | ocpi.platform       | /home/user/opencpi/projects/platform                       | True         |
    | ocpi.osp.avnet      | /home/user/opencpi/projects/osps/ocpi.osp.avnet            | True         |
    --------------------------------------------------------------------------------------------------
    ~~~
<br>

<a name="enable-opencpi-hdl-control-plane"></a>

## Enable OpenCPI HDL Control Plane
---------------------------------------------------------------------------------------------------
<a name="configure-ps-for-opencpi-hdl-control-plane"></a>

### Configure PS for OpenCPI HDL Control Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Configure the PS core IP to enable and expose the following signals/ports and configure the address of the memory mapped, per the requirements of the OpenCPI HDL Control Plane control software for the Zynq-7000 devices:
    - Clock(s)
    - Reset(s)
    - Master memory map interface
        - Address configured for 0x4000_0000

**NOTE:** As a result of applying the 'Preset', all default settings of the PS core IP are accepted unless specified in the steps below.  

1. These steps continue with the completion of the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section
2. Open the Vivado "ps7_microzed_7020_preset" project, File -> Project -> Save As ->  "ps7_microzed_7020_ocpi_cp"
    - **CRITICAL NOTE:  A similar step will be performed when developing support for the DP. This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
3. Open the Block Design, and if not already enabled, configure the PS core IP to enable the M_AXI_GP0 port
    1. Re-customize the PS IP module by double-clicking on the Zynq7 PS core IP block
    2. Page Navigator → PS-PL Configuration → AXI Non Secure Enablement → GP Master AXI Interface → M AXI GP0 interface → checked, and accept the defaults
4. Remove the following connection (it will be re-connected in a subsequent step):
    1. Remove the a connection between the M_AXI_GP0_ACLK and FCLK_CLK0_0
5. Make the following ports external
    1. Select "FCLK_CLK0", then Right-Mouse-Click (RMC) and "Make External"
    2. Select "FCLK_RESET0_N_0", then Right-Mouse-Click (RMC) and "Make External"
    3. Select "M_AXI_GP0", then Right-Mouse-Click (RMC) and "Make External"
6. Now the that port has been made external, an internal connection can be made:
    1. Make a connection from the M_AXI_GP0_ACLK and FCLK_CLK0_0
7. Assign clocks to external ports
    1. Select "FCLK_RESET0_N_0", then in the "External Interface Properties" window, set the "Clock Port" to "FCLK_CLK0_0"
    2. Select "M_AXI_GP0_0", then in the "External Interface Properties" window, set the "Clock Port" to "FCLK_CLK0_0"
    3. Select "FCLK_CLK0_0", then in the "External Interface Properties" window, confirm that the Frequency (MHz) is 100.
8. Perform "Validate Design". (the next step will resolve the Critical Messages)
9. Select "Address Editor" tab and select "Auto Assign Address"
10. Set the "Offset Address" per the requirements of the OpenCPI HDL Control Plane, i.e. "0x4000_0000"
11. Return to "Diagram" and "Validate Design" (F6)
12. Flow Navigator window → IP INTEGRATOR → Generate Block Design  
    **CRITCAL NOTE: Recall from the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section, that "Create HDL Wrapper" was performed, because it is a prerequiste for performing "Export Hardware".** 
    1. Performing "Generate Block Design", should automatically update the HDL Wrapper file, if the defaults setting was accepted to allow Vivado to auto-manage updating the wrapper.
13. After the Generate Block Design is complete, export the hardware by selecting
    1. File → Export → Export Hardware
    2. DO NOT check the box for "Include Bitstream"
    2. Confirm the "Export to" path is correct
    3. Select "OK"
14. The design should look as follows:  
![Control Plane BD](./images/control_plane_BD.png)  
<br>

<a name="petalinux-workspace-for-control-plane"></a>

### PetaLinux workspace for Control Plane
---------------------------------------------------------------------------------------------------
1. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly.  
2. Create the PetaLinux workspace for the development of Control Plane ONLY SD card image
    $ /home/user/plx_7020_cp  
    **CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section has not produce a bitstream.**  
<br>

<a name="configure-hdl-primitive-for-control-plane"></a>

### Configure HDL Primitive for Control Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Create an OpenCPI HDL primitive that wraps the Zynq-7000 PS core IP which has been configured per the settings of the MicroZed 7020 SOM + FMC Carrier. Since the zed OpenCPI HDL Platform targets the same device family, its HDL primitive module is used as a reference implementation for this task.

**CODE BLOCK:**   
**The code block for the various files that make up the HDL Primitive can be found in the following directory of the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/guide/microzed_cc/Enable_OpenCPI_Control-Plane/Primitive/**

1. Setup terminal for OpenCPI development   
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s
2. Create an OpenCPI HDL primitive library, named "zynq_microzed_20_cc"  
    $ cd projects/osps/ocpi.osp.avnet  
    $ ocpidev create hdl primitive library zynq_microzed_20_cc
3. From the Vivado project modified in [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane) and which is specific to using the "Preset" to configure the PS core IP for the MicroZed 7020, browse to the generated artifacts directory, and copy them into the newly created OpenCPI HDL primitive library.
    1. $ cd opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc
    2. $ cp -rf /home/user/ps7_microzed_7020_ocpi_cp/ps7_microzed_7020_ocpi_cp.srcs/sources_1/bd/preset_config_wrapper/ip/preset_config_wrapper_processing_system7_0_0/ /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc
4. Since the MicroZed is very similar to the Zedboard, we can simply copy and rename a couple files from the platform/hdl/primitive/zynq HDL primitive library into the zynq_microzed_20_cc and edit as needed.
    1. $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc
    2. $ cp /home/user/opencpi/projects/platform/hdl/primitives/zynq/zynq_pkg.vhd /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc/zynq_microzed_20_cc_pkg.vhd
    3. $ cp /home/user/opencpi/projects/platform/hdl/primitives/zynq/zynq_ps.cpp_vhd /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc/zynq_microzed_20_cc_ps.vhd  
    **CRITICAL NOTE:**  
    **This guide does not perform the C++ preprocessing on the zynq_ps.cpp_vhd, that is described in the platform/hdl/primitives/zynq/Makefile. All C++ preprocessing will be removed in subsequent steps.**
    4. $ cp /home/user/opencpi/projects/platform/hdl/primitives/zynq/zynq_sdp.vhd /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc/zynq_microzed_20_cc_sdp.vhd       
5. Edit the "zynq_microzed_20_cc_ps.vhd", to remove all C++ preprocessing code and to normalize the interface of the generated PS7 core IP to OpenCPI Control Plane signaling.
    1. Change library names from: 
        - "library zynq;" to "library zynq_microzed_20_cc;"
        - "use zynq.zynq_pkg.all;" -> "use zynq_microzed_20_cc.zynq_microzed_20_cc_pkg.all; "
    2. Change entity name from "zynq_ps" to "zynq_microzed_20_cc_ps"
    3. Change package_name to clg400 (For the MicroZed 7020)
    4. Comment out the HP and ACP ports from the entity (these are for the Data Plane, and will be added back later)
    5. Change architecture name from "zynq_ps" to "zynq_microzed_20_cc_ps"
    6. Remove all ifndef code-block statements
    7. Replace the PS7_WRAPPER_MODULE that is in the architecture declaration and body with the the PS7 core IP primitive from the "Preset" project
        1. Start by opening the preset_config_wrapper_processing_system7_0_0/preset_config_wrapper_processing_system7_0_0_stub.vhdl to observed only those signals which need to remain in the "zynq_microzed_20_cc_ps.vhd"
        2. i.e Change component and instance from "PS7_WRAPPER_MODULE" to "preset_config_wrapper_processing_system7_0_0", remove all generics and signals that are not shown in the preset_config_wrapper_processing_system7_0_0_stub.vhdl  
        **NOTE: This code block is built using the Preset Project. If you are building using the "Reference Design" method make sure that you use the correct component name of MZ7020_FMCCC_ps7_0 and include the SDIO0_WP Signals. Comments have been placed accordingly.**
    8. Add the following constant delcarations:
    ~~~
        constant C_M_AXI_GP0_THREAD_ID_WIDTH : natural := 12;
        constant C_MIO_PRIMITIVE             : natural := 54;
        constant C_DM_WIDTH                  : natural := 4;
        constant C_DQ_WIDTH                  : natural := 32;
        constant C_DQS_WIDTH                 : natural := 4; 
    ~~~        
6. Edit the HDL package "zynq_microzed_20_cc_pkg.vhd"  
    1. Change package name from "zynq_pkg" to "zynq_microzed_20_cc_pkg"
    2. Change primitive component name from "zynq_ps" to "zynq_microzed_20_cc_ps"
    3. Change sdp component name from "zynq_sdp" to "zynq_microzed_20_cc_sdp"
    4. Where appropriate, change the "package_name" value to "clg400"  
    **CRITICAL NOTE:**
    **Depending on which platform is being targeted the 'package_name : string := "part_number" ' will be different. The Code Block is an example of the MicroZed 7020 which has the clg400 part number designation. Keep this in mind thoughout the document.**
    5. In the primitive and sdp component declarations, comment out the HP and ACP ports (these are for the Data Plane, and will be added back later)
7. Edit the HDL SDP module "zynq_microzed_20_cc_sdp.vhd"  
    1. Change library names from: 
        - "library zynq;" to "library zynq_microzed_20_cc;"
        - "use zynq.zynq_pkg.all;" -> "use zynq_microzed_20_cc.zynq_microzed_20_cc_pkg.all; "
    2. Change entity name from "zynq_sdp" to "zynq_microzed_20_cc_sdp"
    3. Change package_name to clg400 (For the MicroZed 7020)  
    **CRITICAL NOTE:**
    **Depending on which platform is being targeted the 'package_name : string := "part_number" ' will be different. The Code Block is an example of the MicroZed 7020 which has the clg400 part number designation. Keep this in mind thoughout the document.**
    4. Change architecture name from "zynq_sdp" to "zynq_microzed_20_cc_sdp"
    5. Comment out the HP and ACP ports and signals (these are for the Data Plane, and will be added back later)
    6. Since the PS core IP implements a BUFG on the output of the FCLK_CLK0, the BUFG that is instanced ("clkbuf") in the SDP module is redundant and the build tools will flag this as a WARNING. Therefore the BUFG in the SDP module must be removed. Examine the ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc/zynq_microzed_20_cc_sdp.vhd for the necessary changes to bypass "clkbuf".
8. Update the primitive library's Makefile to specify all of the dependencies:  
    /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc/Makefile  
9. Return to the top of the project and build the primitive library:  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet  
    $ ocpidev build --hdl-target zynq
    ~~~
    $ ocpidev build --hdl-target zynq
    No HDL platforms specified.  No HDL assets will be targeted.
    Possible HdlPlatforms are: alst4 alst4x isim matchstiq_z1 ml605 modelsim x4sim xsim zcu104 zed zed_ise.
    make[1]: Entering directory `/home/user/opencpi/projects/osps/ocpi.osp.avnet'
    make[1]: Leaving directory `/home/user/opencpi/projects/osps/ocpi.osp.avnet'
    ============== For library zynq_microzed_20_cc:
    Building the zynq_microzed_20_cc library for zynq (target-zynq/zynq_microzed_20_cc) 0:()
     Tool "vivado" for target "zynq" succeeded.  0:00.02 at 19:20:45
    Creating directory ../lib/zynq_microzed_20_cc for library zynq_microzed_20_cc
    No previous installation for gen/zynq_microzed_20_cc.libs in ../lib/zynq_microzed_20_cc.
    Installing gen/zynq_microzed_20_cc.libs into ../lib/zynq_microzed_20_cc
    No previous installation for target-zynq/zynq_microzed_20_cc.sources in target-zynq/zynq_microzed_20_cc.
    Installing target-zynq/zynq_microzed_20_cc.sources into target-zynq/zynq_microzed_20_cc
    No previous installation for target-zynq/zynq_microzed_20_cc in ../lib/zynq_microzed_20_cc/zynq.
    Installing target-zynq/zynq_microzed_20_cc into ../lib/zynq_microzed_20_cc/zynq
    ~~~
<br>

<a name="configure-hdl-platform-worker-for-control-plane"></a>

### Configure HDL Platform Worker for Control Plane

---------------------------------------------------------------------------------------------------
**CODE BLOCK: The code block for the various files that make up the HDL platform worker can be found in the following directory of the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/guide/microzed_cc/Enable_OpenCPI_Control-Plane/Platform-Worker/**

1. Create HDL Platform Worker:  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet  
    $ ocpidev create hdl platform microzed_20_cc    
2. Change directory to HDL Platform Worker  
    $ cd hdl/platforms/microzed_20_cc
3. Copy zed.xml into the microzed_20_cc directory and rename it microzed_20_cc.xml, and edit
    1. $ cp /home/user/opencpi/projects/platform/hdl/platforms/zed/zed.xml /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/microzed_20_cc.xml
    2. Change the name of the platform from "zed" to "microzed_20_cc"  
        \<SpecProperty Name='platform' Value='microzed_20_cc'/>
    3. Comment out the sdp module
    4. Comment out SpecProperties: nSlots, slotNames, sdp_width, only the following SpecProperty shall remain:  
        \<SpecProperty Name='platform' Value='microzed_20_cc'/>
    5. Comment out Properties: sdp_channels, use_acp axi_error, sdpDropCount, all debug properties, only the following property shall remain:  
        \<property name='useGP1' type='bool' parameter='1' default='false'/>
    6. Comment out signals: led and fmc_prsnt
    7. Comment out the slot declaration
    8. Add the following ports:  
        \<signal output='PL_LED1'/>  
        \<signal output='PL_LED2'/>  
        \<signal output='PL_LED3'/>  
        \<signal output='PL_LED4'/>  
4. Copy zed.vhd into the microzed_20_cc directory and rename it microzed_20_cc.vhd, and edit
    1. $ cp /home/user/opencpi/projects/platform/hdl/platforms/zed/zed.vhd /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/microzed_20_cc.vhd
    2. Library "zynq" → "zynq_microzed_20_cc"
    3. Change name of instance "zynq_sdp" → "zynq_microzed_20_cc_sdp"
    4. Comment out all Data Plane related circuitry: any signal with "sdp" or "hp"
    5. Comment assignments: sdpDropCount, slotCardIsPresent, led*
    6. Add port assignments:  
        PL_LED1 <= count(count'left);  
        PL_LED2 <= count(count'left-1);  
        PL_LED3 <= reset;  
        PL_LED4 <= '1';  
 5. Create microzed_20_cc.xdc
    1. Copy the constraints file from the MicroZed vendor reference design into the microzed_20_cc Platform Worker and rename it "microzed_20_cc.xdc"  
        $ cp /home/user/vendor_reference_design/"Z7010 or Z7020 MicroZed with MBCC-FMC-PCB-B_v2.xdc" /home/user/opencpi/projects/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/  
        $ cp "Z7010 or Z7020 MicroZed with MBCC-FMC-PCB-B_v2.xdc" microzed_20_cc.xdc  
        **NOTE:** Keep a copy of the original vendor provide xdc file
    2. Edit the microzed_20_cc.xdc to
        1. Add the appropriate clock constraint
        2. Comment out, but do not delete as some will be reintroduced later, all of the signals except the "PL User LEDs", "PL Clock Input - Bank 13 / JX3" and the "IOSTANDARD Constraints" sections.
6. Copy the zed/Makefile into /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/Makefile and edit such that its contents match the provided CODE BLOCK
7. Copy/rename the zed/zed.mk into /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/microzed_20_cc.mk and edit such that its contents match the provided CODE BLOCK
8. Copy/rename the zed/zed.exports into /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/microzed_20_cc.exports and edit such that its contents match the provided CODE BLOCK
7. Copy/rename the zed/98-zedboard.rules into /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/98-microzed_20_cc.rules and edit such that its contents match the provided CODE BLOCK
9. Create a microzed_20_cc_bit.xdc, which is to remain empty. By remaining empty, this signifiying that all defaults are used by Vivado.
10. Create "sd_card" directory
11. Copy /home/user/opencpi/platforms/zynq/zynq_system.xml into the sd_card/ and rename "system.xml" and edit the system.xml to look like the following:
    ~~~
    <opencpi>
        <container>
            <rcc load='1'/>
            <remote load='1'/>
            <hdl load='1'>
                <device name='PL:0' platform='microzed_20_cc'/>
            </hdl>
        </container>
        <transfer smbsize='128K'>
            <pio load='1' smbsize='10M'/>
            <dma load='1'/>
            <socket load='1'/>
        </transfer>
    </opencpi>
    ~~~  
<br>

<a name="build-the-hdl-platform-with-control-plane-enabled"></a>

### Build the HDL Platform with Control Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Build the HDL Platform Worker and "base" Platform Configuration
- Verify that the HDL platform is recognized by the framework

1. Build the HDL platform microzed_20_cc  
    $ cd /home/user/opencpi/projects/ocpi.osp.avnet  
    $ ocpidev build --hdl-platform microzed_20_cc  
2. Confirm that the microzed_20_cc is recognized by the framework as a valid HDL platform target:  
    $ ocpidev show platforms  
    ~~~
    ------------------------------------------------------------------------------------------------------------------------------------------------
    | Platform           | Type | Package-ID                                   | Target                  | HDL Part                   | HDL Vendor |
    | ------------------ | ---- | -------------------------------------------- | ----------------------- | -------------------------- | ---------- |
    | centos7            | rcc  | ocpi.core.platforms.centos7                  | linux-c7-x86_64         | N/A                        | N/A        |
    | xilinx13_3         | rcc  | ocpi.core.platforms.xilinx13_3               | linux-x13_3-arm         | N/A                        | N/A        |
    | microzed_20_cc     | hdl  | ocpi.osp.avnet.platforms.microzed_20_cc      | zynq                    | xc7z020-1-clg400           | xilinx     |
    | zed                | hdl  | ocpi.platform.platforms.zed                  | zynq                    | xc7z020-1-clg484           | xilinx     |
    ~~~    
<br>

<a name="hdl-control-plane-verification-read-opencpi-magic-word"></a>

### HDL Control Plane verification: read OpenCPI Magic Word
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to verify that the implementation of the HDL Control Plane has enabled the ability for the user to read the "Magic Word" from the board. This "Magic Word" spells out "CPIxxOPEN" in hex and is a first simple validation step that the OpenCPI HDL Control Plane is functioning correctly. Furthermore, as this step only requires devmem to be present on the SD card image, there is no need for the OpenCPI run-time utilities to be cross-compilied.  

Build HDL Control Plane-ONLY Assembly
1. Setup terminal for OpenCPI development
2. Build the pattern_capture_asm assembly for the microzed_20_cc platform  
    $ cd /home/user/opencpi/projects/assets/hdl/assemblies/pattern_capture_asm directory  
    $ ocpidev build --hdl-platform microzed_20_cc
3. Once built, navigate to the container-pattern_capture.../target-zynq directory and take note of the directory and the pattern_capture_asm_microzed_20_cc_base.bit file.  
**CRITICAL NOTE: For this verification, the bitstream MUST be packaged into the BOOT.BIN, so that it is loaded during u-boot. Steps for including the bitstream in the BOOT.BIN are detailed below.**

Package the bitstream into the BOOT.BIN
1. In another terminal window, setup for developing with Petalinux 2019.2  
    $ source /opt/pkg/petalinux/2019.2/settings.sh
2. Change into the PetaLinux workspace directory for Control Plane ONLY development  
    $ cd /home/user/plx_7020_cp
3. Force the pattern_capture_asm_microzed_20_cc_base.bit bitstream to be include in the BOOT.BIN  
    - Navigate to the /home/user/plx_7020_cp/images/linux directory and perform the following command:  
        $ petalinux-package --boot --fsbl --fpga /home/user/opencpi/projects/assets/hdl/assemblies/pattern_capture_asm/container-pattern_capture_asm_microzed_20_cc_base/target-zynq/pattern_capture_asm_microzed_20_cc_base.bit --u-boot --force
4. Move or remove the contents from within the micro-SD card to be sure that it is empty.
5. Copy the BOOT.BIN and image.ub into an micro-SD card that is configured for FAT32 and has (1) partition.  
    - Link to partition SD-card: https://ragnyll.gitlab.io/2018/05/22/format-a-sd-card-to-fat-32linux.html  
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
6. Install the SD card into the MicroZed, power on the MicroZed, and notice that the blue LED illuninates indicating that the FPGA is programmed during u-boot
7. Open a serial connection to observe the successful booting of the Embedded Linux  
    $ sudo screen /dev/ttyUSB0 115200
8. Petalinux Login:  
    plx_7020_cp login: root  
    Password: root  

Read the OpenCPI "Magic Word" using devmem
1. Read the OpenCPI "Magic Word"  
    Perform the following commands:  
    1. root@plx_7020_cp:~# devmem 0x40000000  
        Output: 0x4F70656E  
    2. root@plx_7020_cp:~# devmem 0x40000004  
        Output: 0x43504900  
    3. These two output lines translate to the following:  
        https://codebeautify.org/hex-string-converter  
        Coverter Input: 435049004f70656e  
        Converter Output: CPIxxOpen  
        
Cleanup - Remove the bitstream from within the BOOT.BIN  
- Prior to moving on to the next step, update the BOOT.BIN such that NO bitstream is included, otherwise setting up for Server Mode will have issues. Server Mode is used for running Component Unit Tests during the Data Plane validation section.
    1. Navigate to the /home/user/plx_7020_cp/images/linux  directory and perform the following command:  
    $ petalinux-package --boot --fsbl --u-boot --force
    2. Copy the BOOT.BIN and image.ub into an micro-SD card   
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. Install the SD card into the MicroZed, power on the MicroZed, and notice that the blue led does NOT illuninate, indicating that the FPGA is NOT programmed, as desired.  
<br>

<a name="install-and-deploy-the-hdl-platform-with-control-plane-enabled"></a>

### Install and Deploy the HDL Platform with Control Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Setup reference directories /opt/Xilinx/ZynqReleases and /opt/Xilinx/git that are used by the OpenCPI cross-compiler during the ocpiadmin install and ocpiadmin deploy commands
- Use the formal OpenCPI platform install and deploy commands to produce the artifacts necessary to populate an SD card.
- Verify that the Control-Plane enabled configuration has been successfully implemented

1. Setup the cross-compiler 
    - The following commands are outlined in the OpenCPI Installation Guide here:  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf (PG. 57)   
    1. ZynqReleases implementation  
        $ sudo mkdir -p /opt/Xilinx/ZynqReleases/2019.2/  
        $ mkdir /home/user/2019.2-microzed_20_cc-release  
        **CRITICAL NOTE: This directory name must have the form: \<tool version>-\<bsp_name>-release**   
        $ cd /home/user/plx_7020_cp/images/linux    
        $ cp BOOT.BIN image.ub /home/user/2019.2-microzed_20_cc-release  
        $ cd /home/user/  
        $ tar cvfz 2019.2-microzed_20_cc-release.tar.xz 2019.2-microzed_20_cc-release  
        $ sudo cp 2019.2-microzed_20_cc-release.tar.xz /opt/Xilinx/ZynqReleases/2019.2  
        $ sudo chown -R \<user>:users /opt/Xilinx/ZynqReleases 
        - Example: sudo chown -R smith:users /opt/Xilinx/ZynqReleases   
        **NOTE:** This may require adjusting the permissions for /opt/Xilinx/ZynqReleases or its subdirectories.        
    2. git implementation  
        $ sudo mkdir -p /opt/Xilinx/git  
        $ cd /opt/Xilinx/git  
        $ sudo git clone --branch xilinx-v2019.2.01 https://github.com/Xilinx/linux-xlnx.git (TYPO: in OCPI doc xilinx_v2019.2)  
        $ sudo git clone --branch xilinx-v2019.2 https://github.com/Xilinx/u-boot-xlnx.git  (TYPO: in OCPI doc xilinx_v2019.2)  
        $ sudo chown -R \<user>:users /opt/Xilinx/git 
        - Example: sudo chown -R smith:users /opt/Xilinx/git   
        **NOTE:** This may require adjusting the permissions for /opt/Xilinx/git or its subdirectories.  
        
2. Install microzed_20_cc (hdl-platform) and xilinx19_2_aarch32 (rcc-platform) 
    **NOTE:** Before installing these platforms, double-check to be sure that all steps have been implemented in the [Modifications to the Install and Deploy scripts](#modifications-to-the-install-and-deploy-scripts) section.
    1. Install microzed_20_cc (hdl-platform)  
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform microzed_20_cc  
    2. Install xilinx19_2_aarch32 (rcc-platform)    
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform xilinx19_2_aarch32 

3. Deploy: microzed_20_cc with xilinx19_2_aarch32
    1. Execute the deployment (with "verbose")  
        $ cd /home/user/opencpi  
        $ ocpiadmin deploy -v platform xilinx19_2_aarch32 microzed_20_cc  

4. Verify the contents of the sd-card components  
**NOTE:** While your md5sum's and the guides md5sum may differ, you should verify that the two directories below contain the same md5sum's on **your** system.
    1. Check the md5sum of the sd-card components  
        $ cd /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32    
        $ md5sum BOOT.BIN    
        868648b7df8c605550003e930cd7dade  BOOT.BIN
        $ md5sum image.ub   
        27f405862d963b570dc2386fff7c6e24  image.ub
    2. Check the md5sum of the plx_7020_cp  
        $ cd /home/user/plx_7020_cp/image/linux  
        $ md5sum BOOT.BIN   
        868648b7df8c605550003e930cd7dade  BOOT.BIN (MATCH)   
        $ md5sum image.ub   
        27f405862d963b570dc2386fff7c6e24  image.ub (MATCH)   

5. Copy files to the SD card
    1. cd /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32
    2. cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. $ cp -RLp /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/ /run/media/\<user>/\<sd-card>/ 

6. At boot-up, verify that the system is running the Control Plane configuration (plx_7020_cp)
    ~~~
    plx_7020_cp login: root
    Password:
    root@plx_7020_cp:~
    ~~~
<br>

<a name="hdl-control-plane-verification-test-application-pattern_capture"></a>

### HDL Control Plane verification: test application pattern_capture
---------------------------------------------------------------------------------------------------
GOAL:
- Run the HDL Control Plane application on the embedded platform

**NOTE**: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all microzed board variations.

1. Use the SD card created in [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section.
2. Install the SD card into the MZCC and apply power.
3. Establish a serial connection from the Host to the MZCC, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
4. Petalinux Login:  
    plx_7020_cp login: root  
    Password: root  
5. Mount the "opencpi" directory
~~~
root@plx_7020_cp:~/# mkdir opencpi  
root@plx_7020_cp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
root@plx_7020_cp:~/# cd opencpi/  
root@plx_7020_cp:~/opencpi# ls
COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
default_mysetup.sh     release                xilinx19_2_aarch32     zynq_setup_common.sh
VERSION                default-system.xml     mysetup.sh             scripts                
zynq_net_setup.sh
~~~
6. Edit the Standalone Mode scripts to load the Control Plane ONLY bitstream
    1. Make a copy of "default_mysetup.sh" named "mysetup.sh"  
        root@plx_7020_cp:~/opencpi# cp default_mysetup.sh mysetup.sh
    2. Edit the "mysetup.sh" to load the pattern_capture_asm bitstream:  
        FROM:  
        if ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/testbias_$HDL_PLATFORM\_base.bitz; then  
        
        TO:  
        if ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/pattern_capture_asm_$HDL_PLATFORM\_base.bitz; then
7. Setup the embedded platform environment for Standalone Mode by sourcing the Standalone Mode setup script mysetup.sh
    1. root@plx_7020_cp:~/# cd /opencpi
    2. root@plx_7020_cp:~/opencpi# source /home/root/opencpi/mysetup.sh
    3. Notice the blue LED illuninates to indicate that the FPGA has been loaded with the pattern_capture_asm bitstream and the stdout reports valid container detection for the ARM and PL (shown below)
8. Run application  
    % cd /home/root/opencpi/applications/  
    % export OCPI_LIBRARY_PATH=../artifacts  
    % ocpirun -v -d pattern_capture.xml
9. stdout of screen session:
~~~
root@plx_7020_cp:~/opencpi# source /home/root/opencpi/mysetup.sh
Attempting to set time from time.nist.gov
rdate: bad address 'time.nist.gov'
====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
Running login script.
OCPI_CDK_DIR is now /run/media/mmcblk0p1/opencpi.
OCPI_ROOT_DIR is now /run/media/mmcblk0p1/opencpi/...
Executing /home/root/.profile.
No reserved DMA memory found on the linux boot command line.
opencpi: loading out-of-tree module taints kernel.
NET: Registered protocol family 12
Driver loaded successfully.
OpenCPI ready for zynq.
Loading bitstream
Bitstream loaded successfully
Discovering available containers...
Available containers:
 #  Model Platform            OS     OS-Version  Arch     Name
 0  hdl   microzed_20_cc                                   PL:0
 1  rcc   xilinx19_2_aarch32  linux  19_2        aarch32  rcc0
% cd /home/root/opencpi/applications/
% export OCPI_LIBRARY_PATH=../artifacts
% ocpirun -v -d pattern_capture.xml
Available containers are:  0: PL:0 [model: hdl os:  platform: microzed_20_cc], 1: rcc0 [model: rcc os: linux platform: xilinx19_2_aarch32]
Actual deployment is:
  Instance  0 pattern_v2 (spec ocpi.assets.util_comps.pattern_v2) on hdl container 0: PL:0, using pattern_v2/a/pattern_v2 in ../artifacts/pattern_capture_asm_microzed_20_cc_base.bitz dated Wed May 26 12:27:30 2021
  Instance  1 capture_v2 (spec ocpi.assets.util_comps.capture_v2) on hdl container 0: PL:0, using capture_v2/a/capture_v2 in ../artifacts/pattern_capture_asm_microzed_20_cc_base.bitz dated Wed May 26 12:27:30 2021
Application XML parsed and deployments (containers and artifacts) chosen [0 s 38 ms]
Application established: containers, workers, connections all created [0 s 12 ms]
Dump of all initial property values:
Property   0: pattern_v2.dataRepeat = "true" (cached)
Property   1: pattern_v2.numMessagesMax = "5" (parameter)
Property   2: pattern_v2.messagesToSend = "5"
Property   3: pattern_v2.messagesSent = "0"
Property   4: pattern_v2.dataSent = "0"
Property   5: pattern_v2.numDataWords = "15" (parameter)
Property   6: pattern_v2.numMessageFields = "2" (parameter)
Property   7: pattern_v2.messages = "{4,251},{8,252},{12,253},{16,254},{20,255}" (cached)
Property   8: pattern_v2.data = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14" (cached)
Property  20: capture_v2.stopOnFull = "true" (cached)
Property  21: capture_v2.metadataCount = "0"
Property  22: capture_v2.dataCount = "0"
Property  23: capture_v2.numRecords = "256" (parameter)
Property  24: capture_v2.numDataWords = "1024" (parameter)
Property  25: capture_v2.numMetadataWords = "4" (parameter)
Property  26: capture_v2.metaFull = "false"
Property  27: capture_v2.dataFull = "false"
Property  28: capture_v2.stopZLMOpcode = "0" (cached)
Property  29: capture_v2.stopOnZLM = "false" (cached)
Property  30: capture_v2.stopOnEOF = "true" (cached)
Property  31: capture_v2.totalBytes = "0"
Property  32: capture_v2.metadata = "{0}"
Property  33: capture_v2.data = "0"
Application started/running [0 s 11 ms]
Waiting for application to finish (no time limit)
Application finished [0 s 0 ms]
Dump of all final property values:
Property   0: pattern_v2.dataRepeat = "true" (cached)
Property   2: pattern_v2.messagesToSend = "0"
Property   3: pattern_v2.messagesSent = "5"
Property   4: pattern_v2.dataSent = "15"
Property   7: pattern_v2.messages = "{4,251},{8,252},{12,253},{16,254},{20,255}" (cached)
Property   8: pattern_v2.data = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14" (cached)
Property  20: capture_v2.stopOnFull = "true" (cached)
Property  21: capture_v2.metadataCount = "5"
Property  22: capture_v2.dataCount = "15"
Property  26: capture_v2.metaFull = "false"
Property  27: capture_v2.dataFull = "false"
Property  28: capture_v2.stopZLMOpcode = "0" (cached)
Property  29: capture_v2.stopOnZLM = "false" (cached)
Property  30: capture_v2.stopOnEOF = "true" (cached)
Property  31: capture_v2.totalBytes = "60"
Property  32: capture_v2.metadata = "{4211081220,2990856306,2990856306,40},{4227858440,2990856521,2990856521,40},{4244635660,2990856736,2990856521,40},{4261412880,2990856950,2990856736,40},{4278190100,2990857165,2990856950,40},{0}"
Property  33: capture_v2.data = "0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0"
~~~  
10. Verify that the data has successfully pass through the application by consulting the /home/user/projects/assets/applications/pattern_capture/README file  
<br>

<a name="enable-opencpi-hdl-data-plane"></a>

## Enable OpenCPI HDL Data Plane
---------------------------------------------------------------------------------------------------
<a name="configure-ps-for-opencpi-data-plane"></a>

### Configure PS for OpenCPI Data Plane
---------------------------------------------------------------------------------------------------
GOALS:
- Modify the PS core IP from the [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane) section, to add the ports necessary to support enabling the OpenCPI Data Plane
    - Slave High Performance streaming interface(s)
- Re-generate the PS core IP output products for updating the Petalinux project to create an SD-Card Linux image
- Build, Run/Verify the "canary" Data Plane application, "testbias"
- Build, Run/Verify another application which requires the Data Plane, i.e. FSK filerw
- Build, Run/Verify all Component Unit Tests within the core, assets and assets_ts projects.

**NOTE:** All default settings of the PS core IP as a result of applying the 'Preset', are accepted, unless specified in the steps below.  

1. These steps continue with the completion of the [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane) section.   
2. Open the Vivado "ps7_microzed_7020_ocpi_cp" project, File -> Project -> Save As ->  "ps7_microzed_7020_ocpi_dp" -> Ensure that the "Create project subdirectory" box is checked, and confirm the "Project Loacation" is in the desired directory (/home/user/) level prior to clicking "OK". This will provide a seperate project for configuring the PS core IP to support the DP only OSP. 
    - **CRITICAL NOTE: This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
3. Enable the slave high performance ports of the PS core IP
    1. Open the Block Design
    2. Double-click the "processing_system7_0" block to 'Re-customize IP'
    3. Click on PS-PL Configuration 
        1. HP Slave AXI Interface → and enable the four (0-3) "S AXI HPx interfaces" by checking each of their boxes
        2. ACP Slave AXI Interface → and enable the "S AXI ACP interface" by checking its box, and checking the box to "Tie off AxUSER"
        3. Click OK
    3. Make the following ports external by selecting them and pressing CTRL + T:
        1. S_AXI_HPx_FIFO_CTRL (0-3)
        2. S_AXI_ACP
        3. S_AXI_HPx (0-3)
        4. S_AXI_ACP_ACLK
        5. S_AXI_HPx_ACLK (0-3)
    4. Assign clocks to external ports
        1. Select "S_AXI_ACP_0", then in the "External Interface Properties" window, set the "Clock Port" to "S_AXI_ACP_ACLK_0"
        2. Select "S_AXI_HP0_0", then in the "External Interface Properties" window, set the "Clock Port" to "S_AXI_HP0_ACLK_0"
        3. Select "S_AXI_HP1_0", then in the "External Interface Properties" window, set the "Clock Port" to "S_AXI_HP1_ACLK_0"
        4. Select "S_AXI_HP2_0", then in the "External Interface Properties" window, set the "Clock Port" to "S_AXI_HP2_ACLK_0"
        5. Select "S_AXI_HP3_0", then in the "External Interface Properties" window, set the "Clock Port" to "S_AXI_HP3_ACLK_0"
        6. Select "S_AXI_ACP_ACLK_0", then in the "External Interface Properties" window, confirm that the Frequency (MHz) is 100.
        7. Select "S_AXI_HP*_ACLK_0", then in the "External Interface Properties" window, confirm that the Frequency (MHz) is 100.
    5. Select the "Address Editor" tab
        1. LMC -> Auto Assign Addresses
        2. Confirm that the "Offset Address" for the M_AXI_GP0_0 is configured per the requirements of the OpenCPI HDL Control Plane = "0x4000_0000"
    6. Return to "Diagram" tab
    7. "Validate Design" (F6)
        - Ignore the two CLK_DELAY warnings
    8. Flow Navigator window → IP INTEGRATOR → Generate Block Design  
        **CRITCAL NOTE:**  
        **Recall from the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section, that "Create HDL Wrapper" was performed, because it is a prerequiste for performing "Export Hardware".**  
        1. Performing "Generate Block Design", should automatically update the HDL Wrapper file.
    9. After the Generate Block Design is complete, export the hardware by selecting
        1. File → Export → Export Hardware
        2. DO NOT check the box for "Include Bitstream"
        2. Confirm the "Export to" path is correct
        3. Select "OK"
    10. The design should look as follows:  
        ![Data Plane BD](./images/data_plane_BD.png)  
<br>

<a name="petalinux-workspace-for-data-plane"></a>

### PetaLinux workspace for Data Plane
---------------------------------------------------------------------------------------------------
1. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly.  
2. Create a PetaLinux workspace for the development of Data Plane SD card image  
    $ /home/user/plx_7020_dp  
    **CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section has not produce a bitstream.**  
<br>

<a name="configure-hdl-primitive-for-data-plane"></a>

### Configure HDL Primitive for Data Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Edit the OpenCPI HDL primitive library source to support the High-Performance (HP) ports that were made available to the Zynq Processing System above.  

    **CODE BLOCK: The "finalized" code block of various files mentioned in this section can be found at:**
    - **ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc** 

1. At the start of this effort, perform a clean within the OSP directory to ensure that no stale files exist  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet  
    $ ocpidev clean
2. To avoid stale content, remove the current PS core IP before copying over the updated version
    $ cd /home/user/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc  
    $ rm -rf preset_config_wrapper_processing_system7_0_0/
3. From [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane), copy the updated preset_config_wrapper_processing_system7_0_0 directory into the ocpi.osp.avnet HDL primitive directory  
    $ cp -rf /home/user/ps7_microzed_7020_ocpi_dp/ps7_microzed_7020_ocpi_dp.srcs/sources_1/bd/preset_config_wrapper/ip/preset_config_wrapper_processing_system7_0_0/ /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/primitives/zynq_microzed_20_cc  
4. Edit the zynq_microzed_20_cc_pkg.vhd file to enable the newly added HP and ACP Ports.
5. Edit the zynq_microzed_20_cc_ps.vhd file to include the newly enabled HP and ACP Ports
6. Edit the zynq_microzed_20_cc_sdp.vhd file to include the newly enabled HP and ACP Ports.  
<br>

<a name="configure-hdl-platform-worker-for-data-plane"></a>

### Configure HDL Platform Worker for Data Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Edit the HDL Platform Worker files in order to implement the High-Performance (HP) and ACP ports that were made available to the Zynq Processing System.  

    **CODE BLOCK: The codeblock of various files mentioned in this section can be found at:**
    - **ocpi.osp.avnet/guide/microzed_cc/Enable_OpenCPI_Data-Plane/Platform-Worker/** 

1. Edit the microzed_20_cc.xml file to include the newly created HP and ACP ports.
2. Edit the microzed_20_cc.vhd file to include the newly created HP and ACP ports.  
<br>

<a name="build-the-hdl-platform-with-data-plane-enabled"></a>

### Build the HDL Platform with Data Plane enabled
---------------------------------------------------------------------------------------------------
1. Build the HDL Platform Worker and "base" Platform Configuration  
    $ cd /home/user/projects/osps/ocpi.osp.avnet  
    $ ocpidev build --hdl-platform microzed_20_cc  
<br>

<a name="undo-edits-made-to-validate-hdl-control-plane"></a>

### Undo edits made to validate HDL Control Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Initially in support of validating the HDL platform for Control Plane ONLY, several scripts were modified to build and deploy the "canary" Control Plane bitstream (pattern_capture). The purpose of this section is to revert those changes such that the "canary" Data Plane bitstream (testbias) will be installed (i.e. built) and deployed for the targetted HDL platform.

1. git checkout tools/scripts/deploy-platform.sh
2. git checkout tools/scripts/export-platform-to-framework.sh
3. git checkout tools/scripts/ocpiadmin.sh  
<br>

<a name="install-and-deploy-the-hdl-platform-with-data-plane-enabled"></a>

### Install and Deploy the HDL Platform with Data Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Setup the /opt/Xilinx/ZynqReleases reference directory that is used by the OpenCPI cross-compiler during the ocpiadmin install and ocpiadmin deploy commands
- Clean stale data that may be present in the xilinx19_2_aarch32 rcc-platform so fresh sd-card contents (BOOT.BIN and image.ub) are implemented at the deploy stage.
- Use the formal OpenCPI platform install and deploy commands to produce the artifacts necessary to populate an SD card.
- Verify that the Data-Plane enabled configuration has been successfully implemented


    **NOTE:** The ZynqReleases directory should have been setup in the [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section. In this section we will be replacing the CP BOOT.BIN and image.ub with the updated DP BOOT.BIN and image.ub that was created in the [PetaLinux workspace for Data Plane](petalinux-workspace-for-data-plane) section. The git directory that was created in the  [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section should be unaltered as no changes need to be implemented.  

1. Setup the cross-compiler 
    - The following commands are outlined in the OpenCPI Installation Guide here:  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf (PG. 57)   
    1. ZynqReleases implementation  
        $ rm /opt/Xilinx/ZynqReleases/2019.2/2019.2-microzed_20_cc-release.tar.xz
        $ cd /home/user/plx_7020_dp/images/linux    
        $ cp BOOT.BIN image.ub /home/user/2019.2-microzed_20_cc-release    
        $ cd /home/user/    
        $ tar cvfz 2019.2-microzed_20_cc-release.tar.xz 2019.2-microzed_20_cc-release    
        $ sudo cp 2019.2-microzed_20_cc-release.tar.xz /opt/Xilinx/ZynqReleases/2019.2    
        $ sudo chown -R \<user>:users /opt/Xilinx/ZynqReleases   
        - Example: sudo chown -R smith:users /opt/Xilinx/ZynqReleases   
        **NOTE:** This may require adjusting the permissions for /opt/Xilinx/ZynqReleases or its subdirectories.        

2. Clean stale data in xilinx19_2_aarch32 (rcc-platform)  
    1. Remove xilinx19_2_aarch32  
        $ cd /home/user/opencpi  
        $ rm -rf cdk/xilinx19_2_aarch32  
    2. Clean xilinx19_2_aarch32  
        $ cd /home/user/opencpi/projects/core/rcc/platforms/xilinx19_2_aarch32  
        $ make clean  
        $ rm -rf lib  
    3. Rebuild core exports  
        $ cd /home/user/opencpi/projects/core  
        $ make cleanexports  
        $ make exports  
    
3. Install microzed_20_cc (hdl-platform) and xilinx19_2_aarch32 (rcc-platform))  
    1. Install microzed_20_cc (hdl-platform)  
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform microzed_20_cc  
    2. Install xilinx19_2_aarch32 (rcc-platform)   
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform xilinx19_2_aarch32  

4. Deploy: microzed_20_cc (hdl-platform) with platform xilinx19_2_aarch32 (rcc-platform)
    1. Execute deployment (with "verbose")   
        $ cd /home/user/opencpi  
        $ ocpiadmin deploy -v platform xilinx19_2_aarch32 microzed_20_cc  

5. Verify the contents of the sd-card components  
**NOTE:** While your md5sum's and the guides md5sum may differ, you should verify that the two directories below contain the same md5sum's on **your** system.
    1. Check the md5sum of the sd-card components  
        $ cd /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32    
        $ md5sum BOOT.BIN
        9a7047d25a57fb5035a88a9e9b792038  BOOT.BIN
        $ md5sum image.ub  
        4bd207573d8bba4ecf51002ffccf291f  image.ub
    2. Check the md5sum of the plx_7020_dp  
        $ cd /home/user/plx_7020_dp/image/linux  
        $ md5sum BOOT.BIN  
        9a7047d25a57fb5035a88a9e9b792038  BOOT.BIN (MATCH)  
        $ md5sum image.ub  
        4bd207573d8bba4ecf51002ffccf291f  image.ub (MATCH)

6. Copy files to the SD card
    1. cd /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32
    2. cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. $ cp -RLp /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/ /run/media/\<user>/\<sd-card>/

7. At boot-up, verify that the system is running the Data Plane configuration (plx_7020_dp)
    ~~~
    plx_7020_dp login: root
    Password:
    root@plx_7020_dp:~
    ~~~
<br>

<a name="hdl-data-plane-verification:-testbias-application"></a>

### HDL Data Plane verification: testbias application
---------------------------------------------------------------------------------------------------
GOAL:
- Successfully execute the "canary" HDL Data Plane application on the embedded platform."Success" is defined as the application ran to completion, and the md5sum of the input and output data of the testbias application match, when no bias is being applied to the data, i.e. bias worker property, biasValue=0.

**NOTE**: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all microzed board variations.

1. Use the SD card as a result of the work performed in [Install and Deploy the HDL Platform with Data Plane enabled](#install-and-deploy-the-hdl-platform-with-data-plane-enabled)
2. Install the SD card into the MZCC and apply power.
3. Establish a serial connection from the Host to the MZCC, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
4. Petalinux Login:  
    plx_7020_dp login: root  
    Password: root  
5. Mount the "opencpi" directory
    ~~~
    root@plx_7020_dp:~/# mkdir opencpi  
    root@plx_7020_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_7020_dp:~/# cd opencpi/  
    root@plx_7020_dp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch32     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
6. Setup the embedded platform environment for Standalone Mode by sourcing the appropriate setup script, i.e. `mysetup.sh`   
    root@plx_7020_dp:~/opencpi# cp default_mysetup.sh `mysetup.sh`  
    root@plx_7020_dp:~/opencpi# source /home/root/opencpi/mysetup.sh  
    Notice the blue LED illuninates to indicate that the FPGA has been loaded with the testbias bitstream and the stdout reports valid container detection for the ARM and PL
7. Run application: testbias  
    % cd /home/root/opencpi/applications
    % export OCPI_LIBRARY_PATH=../artifacts:../xilinx19_2_aarch32/artifacts  
    Check that testbias is functioning as expected by verifying that the input and output are equal when assigning a testbias of zero 0 (no change).  
    % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml  
8. stdout of screen session:
    ~~~
    root@plx_7020_dp:~/# mkdir opencpi
    root@plx_7020_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/
    root@plx_7020_dp:~/# cd opencpi/
    root@plx_7020_dp:~/opencpi# cp default_mysetup.sh mysetup.sh    
    root@plx_7020_dp:~/opencpi# source /home/root/opencpi/mysetup.sh
    Attempting to set time from time.nist.gov
    rdate: bad address 'time.nist.gov'
    ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
    Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
    Running login script.
    OCPI_CDK_DIR is now /run/media/mmcblk0p1/opencpi.
    OCPI_ROOT_DIR is now /run/media/mmcblk0p1/opencpi/...
    Executing /home/root/.profile.
    No reserved DMA memory found on the linux boot command line.
    opencpi: loading out-of-tree module taints kernel.
    NET: Registered protocol family 12
    Driver loaded successfully.
    OpenCPI ready for zynq.
    Loading bitstream
    Bitstream loaded successfully
    Discovering available containers...
    Available containers:
    #  Model Platform            OS     OS-Version  Arch     Name
    0  hdl   microzed_20_cc                                   PL:0
    1  rcc   xilinx19_2_aarch32  linux  19_2        aarch32  rcc0
    % pwd
    /run/media/mmcblk0p1/opencpi
    % cd /home/root/opencpi/applications/
    % export OCPI_LIBRARY_PATH=../artifacts:../xilinx19_2_aarch32/artifacts
    % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml
    Available containers are:  0: PL:0 [model: hdl os:  platform: microzed_20_cc], 1: rcc0 [model: rcc os: linux platform: xilinx19_2_aarch32]
    Actual deployment is:
      Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in ../xilinx19_2_aarch32/artifacts/ocpi.core.file_read.rcc.0.xilinx19_2_aarch32.so dated Wed May 26 15:56:54 2021
      Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in ../artifacts/testbias_microzed_20_cc_base.bitz dated Wed May 26 15:56:54 2021
      Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in ../xilinx19_2_aarch32/artifacts/ocpi.core.file_write.rcc.0.xilinx19_2_aarch32.so dated Wed May 26 15:56:54 2021
    Application XML parsed and deployments (containers and artifacts) chosen [0 s 90 ms]
    Application established: containers, workers, connections all created [0 s 22 ms]
    Dump of all initial property values:
    Property   0: file_read.fileName = "test.input" (cached)
    Property   1: file_read.messagesInFile = "false" (cached)
    Property   2: file_read.opcode = "0x0" (cached)
    Property   3: file_read.messageSize = "0x10"
    Property   4: file_read.granularity = "0x4" (cached)
    Property   5: file_read.repeat = "false"
    Property   6: file_read.bytesRead = "0x0"
    Property   7: file_read.messagesWritten = "0x0"
    Property   8: file_read.suppressEOF = "false"
    Property   9: file_read.badMessage = "false"
    Property  16: bias.biasValue = "0x0" (cached)
    Property  20: bias.test64 = "0x0"
    Property  31: file_write.fileName = "test.output" (cached)
    Property  32: file_write.messagesInFile = "false" (cached)
    Property  33: file_write.bytesWritten = "0x0"
    Property  34: file_write.messagesWritten = "0x0"
    Property  35: file_write.stopOnEOF = "true" (cached)
    Property  39: file_write.suppressWrites = "false"
    Property  40: file_write.countData = "false"
    Property  41: file_write.bytesPerSecond = "0x0"
    Application started/running [0 s 2 ms]
    Waiting for application to finish (no time limit)
    Application finished [0 s 60 ms]
    Dump of all final property values:
    Property   0: file_read.fileName = "test.input" (cached)
    Property   1: file_read.messagesInFile = "false" (cached)
    Property   2: file_read.opcode = "0x0" (cached)
    Property   3: file_read.messageSize = "0x10"
    Property   4: file_read.granularity = "0x4" (cached)
    Property   5: file_read.repeat = "false" (cached)
    Property   6: file_read.bytesRead = "0xfa0"
    Property   7: file_read.messagesWritten = "0xfa"
    Property   8: file_read.suppressEOF = "false" (cached)
    Property   9: file_read.badMessage = "false"
    Property  16: bias.biasValue = "0x0" (cached)
    Property  20: bias.test64 = "0x0" (cached)
    Property  31: file_write.fileName = "test.output" (cached)
    Property  32: file_write.messagesInFile = "false" (cached)
    Property  33: file_write.bytesWritten = "0xfa0"
    Property  34: file_write.messagesWritten = "0xfa"
    Property  35: file_write.stopOnEOF = "true" (cached)
    Property  39: file_write.suppressWrites = "false" (cached)
    Property  40: file_write.countData = "false" (cached)
    Property  41: file_write.bytesPerSecond = "0x13954"
    ~~~

    Compare the md5sum of both test.input and test.output  
        % md5sum test.*  
    The Output should be as follows:
    
9. Verify that the data has successfully passed through the application by performing an m5sum on the input and output data files with bias effectively disabled, by setting the biasValue=0
    ~~~
    % md5sum test.*
    2934e1a7ae11b11b88c9b0e520efd978  test.input
    2934e1a7ae11b11b88c9b0e520efd978  test.output
    ~~~
    This shows that with a bias=0 (no change in data) that the input matches the output and the testbias application is working as it should.  
<br>

<a name="hdl-data-plane-verification:-test-fsk-application-filerw-mode"></a>

### **HDL Data Plane verification: test FSK application filerw mode**
---------------------------------------------------------------------------------------------------
GOAL:
- (OPTIONAL) Execute another standard application which relys upon the HDL Data Plane

**NOTE: Instructions for building and running the fsk_filerw on the MicroZed 7010 will be outlined in the [FSK filerw for 7010](#fsk-filerw-for-7010) section in the appendix. The MicroZed 7010 FPGA does not have enough DSP resources to build the standard fsk_filerw worker, so a modified worker is needed.**

**NOTE**: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all microzed board variations.

This section is outlined in the following location: /home/user/opencpi/projects/assets/applications/FSK/doc/FSK_app.tex. This document covers the FSK "txrx" execution mode however, the "filerw" execution mode will be used in this section to validate the implementation of the Data Plane. To retrieve the contents of the FSK_app.tex file install rubber and run the following command:  
$ rubber -d FSK_App_Getting_Started_Guide.tex  
$ envince FSK_App_Getting_Started_Guide.pdf  

Provided here will be a setup guide to building and deploying the FSK application filerw mode:  
**NOTE: $ will signal that the command is performed on the Development host, % will signal that the command is performed on the Target-platform**  

1. Build the FSK Application Executable and copy it into the SD-Card  
    $ cd /home/user/opencpi/projects/assets/applications/FSK  
    $ ocpidev build --rcc-platform xilinx19_2_aarch32  
    $ cd ..  
    $ cp -rf FSK/ /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/applications 
2. Build the fsk_filerw hdl assembly and copy it into the SD-Card  
    $ cd /home/user/opencpi/projects/assets/hdl/assemblies/fsk_filerw
    $ ocpidev build --hdl-platform microzed_20_cc  
    $ cd container-fsk_filerw_microzed_20_cc_base/target-zynq  
    $ cp fsk_filerw_microzed_20_cc_base.bitz /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/artifacts  
3. Implement missing .so files into the sd-card xilinx19_2_aarch32 artifacts directory  
    **NOTE: The following files are needed based off of the /home/user/opencpi/projects/assets/applications/FSK/app_fsk_filerw.xml file under RCC Components. The ocpi.core.file_read is already given but the ocpi.assets.dsp_comps.baudTracking and ocpi.assets.dsp_comps.real_digitizer components need to be supplemented. Check the /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts to see what is included**    
    1. Copy the Baudtracking_simple.so into the SD-Card xilinx19_2_aarch32 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/Baudtracking_simple.rcc/target-xilinx19_2_aarch32  
        $ cp Baudtracking_simple.so /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts/  
    2. Copy the real_digitizer.so into the SD-Card xilinx19_2_aarch32 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/real_digitizer.rcc/target-xilinx19_2_aarch32  
        $ cp real_digitizer.so /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts/  
4. Copy the newly implemented file onto the SD-Card
    $ cp -RLp /home/user/opencpi/cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/opencpi/ /run/media/\<user>/\<sd-card>/
5. Install SD into MicroZed and apply power
6. Connect to the Platform  
    \# sudo screen /dev/tty/USB0 115200  
    Petalinux Login:  
    plx_7020_dp login: root  
    Password: root  
7. Mount the "opencpi" directory
    ~~~
    root@plx_7020_dp:~/# mkdir opencpi  
    root@plx_7020_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_7020_dp:~/# cd opencpi/  
    root@plx_7020_dp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch32     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
8. Setup the embedded platform environment for Standalone Mode by sourcing the appropriate setup script, i.e. `mysetup.sh`  
    root@plx_7020_dp:~/opencpi# cp `default_mysetup.sh` `mysetup.sh`  
    root@plx_7020_dp:~/opencpi# source /home/root/opencpi/mysetup.sh  
    Notice the blue LED illuninates to indicate that the FPGA has been loaded with the testbias bitstream and the stdout reports valid container detection for the ARM and PL
9. Setup the library path to the artifacts  
    % cd /home/root/opencpi/applications/FSK  
    % export OCPI_LIBRARY_PATH=/home/root/opencpi/artifacts:/home/root/opencpi/xilinx19_2_aarch32/artifacts  
10. Run application: FSK filerw  
    % ./target-xilinx19_2_aarch32/FSK filerw  
    ~~~
    % ./target-xilinx19_2_aarch32/FSK filerw
    Application properties are found in XML file: app_fsk_filerw.xml
    App initialized.
    App started.
    Waiting for done signal from file_write.
    real_digitizer: sync pattern 0xFACE found
    App stopped/finished.
    Bytes to file : 8950
    TX FIR Real Peak       = 4696
    Phase to Amp Magnitude = 20000
    RP Cordic Magnitude    = 19481
    RX FIR Real Peak       = 13701
    Application complete
    ~~~
11. In order to see the output of the execution of this application the user needs to copy over the output file located in /run/media/mmcblk0p1/opencpi/applications/FSK/odata/out_app_fsk_filerw.bin over to their development host. In order to do this:
    1. Move the SD to the development host, or
    2. Implement an IP address for the target-platform and scp the out_app_fsk_filerw.bin to the development host.
        1. Implement IP-Address (If it was not setup in [Static IP Address Setup](#static-ip-address-setup))   
            % ifconfig eth0 down  
            % ifconfig eth0 add \<Create Valid ip-address> netmask 255.255.255.0  
            % ifconfig eth0 up  
        2. Secure Copy the output file out_app_fsk_filerw.bin that was produced  
            % cd /run/media/mmcblk0p1/opencpi/applications/FSK/odata  
            % scp out_app_fsk_filerw.bin \<host>@\<host-ip>:\<path>  

12. View the results of the output file      
    $ eog \<path>/out_app_fsk_filerw.bin  
    ![Oriole](./images/oriole.png)  
<br>

---------------------------------------------------------------------------------------------------
<a name="opencpi-component-unit-testing"></a>

## OpenCPI Component Unit Testing
---------------------------------------------------------------------------------------------------

<a name="server-mode-setup"></a>

### Server Mode setup
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to enable the user with the ability to setup the Server Mode. Success of this section is the ability to utilize the ocpiremote utility that enables the Server Mode and provides the ability to load bitstreams from the Client-side (Host) to the Server-side (embedded device).

Enable the MicroZed for remote containers by ensuring its system.xml contains \<remote load='1'> and \<socket load='1'> :
The system.xml can be found and changed in one of two places:  
**NOTE:** Option 1 is more robust than option 2, while option 2 is quicker to implement.
1. /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_20_cc/sd_card/system.xml
    - If it does not match the code-block below, you will have to edit it and rebuild / reinstall / redeploy the platform.
2. /home/user/opencpi/cdk/microzed_20_cc/system.xml
    - If it does not match the code-block below, you can edit it hear and and redeploy the platform, however, if the hdl-platform is deleted and redeployed, this system.xml will not updated.
~~~
<opencpi>
  <container>
    <rcc load='1'/>
    <remote load='1'/>
    <hdl load='1'>
     <device name='PL:0' platform='microzed_20_cc'/>
    </hdl>
  </container>
  <transfer smbsize='128K'>
    <pio load='1' smbsize='10M'/>
    <dma load='1'/>
    <socket load='1'/>
  </transfer>
</opencpi>
~~~

Server-side setup:
1. Establish a serial connection from the Host to the MicroZed, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
2. Petalinux Login:  
    plx_7020_dp login:root  
    Password:root 
3. Network setup: (If it was not setup in [Static IP Address Setup](#static-ip-address-setup))   
    $ ifconfig eth0 down  
    $ ifconfig eth0 add \<Vailid ip-address> netmask 255.255.255.0   
    $ ifconfig eth0 up

Client-side setup:
1. Change to your Opencpi directory:  
    $ cd /home/user/opencpi  
2. Export some Opencpi enviornment variables to discover the remote server:  
    $ export OCPI_SERVER_ADDRESSES=\<Valid ip-address>:\<Valid port>   
    $ export OCPI_SOCKET_INTERFACE=\<Valid socket>  
        "Valid socket" is the name of the Ethernet interface of the development host which can communicate with the MZCC.
3. Load "sandbox" onto the server:  
    $ ocpiremote load -s xilinx19_2_aarch32 -w microzed_20_cc  
    Standard Out:
    ~~~
    $ ocpiremote load -s xilinx19_2_aarch32 -w microzed_20_cc
    Preparing remote sandbox...
    Fri Mar 26 13:09:23 UTC 2021
    Creating server package...
    Sending server package...
    Server package sent successfully.  Getting status (no server expected to be running):
    Executing remote configuration command: status
    No ocpiserve appears to be running: no pid file        
    ~~~
4. Start the Server-Mode:  
    $ ocpiremote start -b  
    Standard Out:  
    ~~~
    $ ocpiremote start -b
    Executing remote configuration command: start -B
    Reloading kernel driver:
    Loading opencpi bitstream
    The driver module is not loaded. No action was taken.
    No reserved DMA memory found on the linux boot command line.
    Driver loaded successfully.
    PATH=/home/root/sandbox/xilinx19_2_aarch32/bin:/home/root/sandbox/xilinx19_2_aarch32/sdk/bin:/usr/bin:/bin
    LD_LIBRARY_PATH=xilinx19_2_aarch32/sdk/lib
    VALGRIND_LIB=
    nohup ocpiserve -v -p 12345 > 20210329-092317.log
    Server (ocpiserve) started with pid: 1428. Initial log is:
    Discovery options: discoverable: 0, loopback: 0, onlyloopback: 0
    Container server at <ANY>:12345
    Available TCP server addresses are:
    On interface eth0: 192.168.0.12:12345
    Artifacts stored/cached in the directory "artifacts", which will be retained on exit.
    Containers offered to clients are:
    0: PL:0, model: hdl, os: , osVersion: , platform: microzed_20_cc
    1: rcc0, model: rcc, os: linux, osVersion: 19_2, platform: xilinx19_2_aarch32
    --- end of server startup log success above
    ~~~
        
5. Firewall Error:  
     - If you are presented with the following error while attempting to run any of the Unit-Tests you may need to disable or reconfigure your firewall:
    ~~~
    OCPI( 2:991.0828): Exception during application shutdown: error reading from container server "": EOF on socket read
    Exiting for exception: error reading from container server "": EOF on socket read
    ~~~  
    You can disable your firewall until it is rebooted with the two following commands:  
    $ sudo systemctl disable firewalld  
    $ sudo systemctl mask --now firewalld  
<br>

<a name="build-and-run"></a>

### Build and Run
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to build and run the framework provided Component Unit Tests. Success of this section is to perform all of the Component Unit Tests making sure that each of the tests "pass" or "fail" as outlined in the [Component Unit Test result table](#component-unit-test-result-table) section. That table compares the 'pass and fail' results compared to the ZedBoard. Please refer to the [Component Unit Test result table](#component-unit-test-result-table) section to ensure that testing behavior is consistent.

**NOTE**: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the Component unit test results of all component unit tests for each of the microzed board variations.

Build the Unit Tests (In parrallel) on the Development Host
1. Build the core component unit tests. (Could take several hours)  
    Open a new terminal on your local machine  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/core  
    $ ocpidev build test --hdl-platform microzed_20_cc --rcc-platform xilinx19_2_aarch32 --rcc-platform centos7  
2. Build the assets component unit tests (Could take several hours)  
    Open a new terminal on your local machine  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/assets  
    $ ocpidev build test --hdl-platform microzed_20_cc --rcc-platform xilinx19_2_aarch32 --rcc-platform centos7  
3. Build the assets_ts component unit tests (Could take several hours)  
    Open a new terminal on your local machine  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/assets_ts  
    $ ocpidev build test --hdl-platform microzed_20_cc --rcc-platform xilinx19_2_aarch32 --rcc-platform centos7  

Run the Unit Tests (Sequentially) on the Development Host
1. Be sure that you have [Server Mode setup](#server-mode-setup) enabled on the MZCC
2. The following article explains the use of the export command below (pg. 108):  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf  
    **NOTE: This section is very painstaking, for the best coverage, the user must traverse into each unit .test directory (and sub-directory for assets) and perform a "runonly" followed by a "verify" of each of the tests in order to verify the full-scope of the Component Unit Tests. Some of these tests are known to fail. A chart is provided in the [Component Unit Test result table](#component-unit-test-result-table) section below that outlines the expected outcome for each of these tests (as of v2.1.0).**  
3. Run the core unit tests  
    $ cd /home/user/opencpi/projects/core/components/*.test  
    $ make runonly OnlyPlatforms=microzed_20_cc  
    $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1
4. Run the assets unit tests  
    $ cd /home/user/opencpi/projects/assets/components/\<sub-directory>/<.test>  
    $ make runonly OnlyPlatforms=microzed_20_cc  
    $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1
5. Run the assets_ts unit tests  
    $ cd /home/user/opencpi/projects/assets_ts/components/<.test>  
    $ make runonly OnlyPlatforms=microzed_20_cc  
    $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1  
<br>

---------------------------------------------------------------------------------------------------
<a name="enable-opencpi-cards-&-slots"></a>

## Enable OpenCPI Cards & Slots
---------------------------------------------------------------------------------------------------
<a name="configure-hdl-platform-worker-with-slot"></a>

### Configure HDL Platform Worker with Slot
---------------------------------------------------------------------------------------------------
**CRITICAL NOTE:**  
**The [FMC Daughter Board Connection Table](#fmc-daughter-board-connection-table) can be used as a reference to understand the FMC organization within the XML file. This was used in conjunction with the schematic of the MZCC and the "Cononical Signal Name" that is outline in the following file /home/user/opencpi/projects/core/hdl/cards/specs/fmc_lpc.xml. Changes had to be made to the constraint file (listed below) in order to adapt the OpenCPI naming convention with the "Cononical Signal Name". This was done referencing the OpenCPI Zed design located /home/user/opencpi/projects/platform/hdl/platforms/zed.** 

**CODE BLOCK:**  
**The "finalized" code block of various files mentioned in this section are located in the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/hdl/platforms/microzed_20_cc** 

1. Edit the microzed_20_cc.xml to include the implementation of the FMC LPC Slot
2. Edit the microzed_20_cc.vhd to include the implementation of the FMC LPC Slot
3. Edit the microzed_20_cc.xdc to include the implementation of the FMC LPC Slot. This includes adding a "FMC_*" prefix to all signals that are specific to the FMC LPC connector.  This is necessary because the "name" attribute was given to the slot declaration in the Platform Worker XML, i.e. <slot name='FMC' type='fmc_lpc'>. Furthermore, this matches the implementation of the zed OSP.
<br>

<a name="building-the-hdl-platform-worker-with-slot-enabled"></a>

### Building the HDL Platform Worker with Slot enabled
---------------------------------------------------------------------------------------------------
$ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet  
$ ocpidev build --hdl-platform microzed_20_cc  
<br>

<a name="install-and-deploy-the-slot-configuration"></a>

### Install and Deploy the Slot configuration
---------------------------------------------------------------------------------------------------
1. Install microzed_20_cc (hdl-platform)  
    $ cd /home/opencpi  
    $ ocpiadmin install platform microzed_20_cc
2. Deploy: microzed_20_cc with xilinx19_2_aarch32  
    $ ocpiadmin deploy platform xilinx19_2_aarch32 microzed_20_cc  
<br>   

<a name="cards-&-slots-verification:-fsk-modem-on-the-fmcomms2"></a>

### Cards & Slots verification: FSK modem on the fmcomms2
---------------------------------------------------------------------------------------------------
GOAL:
- Verify the FMC slot defined in the HDL Platform Worker, by building the appropriate HDL assembly (fsk_modem), running the application (fsk_modem_app) and verifying the output file. In addition to building an HDL assembly, a fmcomms2 or fmcomms3 daugthercard must be install on the MZCC.

**CRITICAL NOTE:**
**The FSK modem verification described here can only be applied to the 7020. This FSK modem verification CANNOT be used to the 7010 due to its resource limittions. If you wish to construct a working verification for the 7010, you can build the fsk_modem_10 hdl assembly located in the /home/user/ocpi.osp.avnet/hdl/assemblies. For help with building the 7010 review the [FSK Modem for 7010](#fsk-modem-for-7010) section.**

**NOTE**: The [Verification test result table](#verification-test-result-table) section in the appendix contains the verfication test results of all microzed board variations.

**CODE BLOCK:**  
**The "finalized" code block of various files mentioned in this section are located in the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/hdl/assemblies/fsk_modem/** 

<a name="building-the-fsk_modem HDL Assembly"></a>

#### Building the fsk_modem HDL Assembly

**The fsk_modem hdl assembly is located in the following directory:**   
- ocpi.osp.avnet/hdl/assemblies/fsk_modem

Container XML and constraint files must be created to include the fmcomms2 daughtercard in the build of the fsk_modem HDL assembly. Once again, the zed is used as a reference. Specifically, within the assets/hdl/assemblies/fsk_modem/, the zed container XML and constraints files will be relied upon for required content
1. Make an HDL assembly directory   
    $ cd /home/user/projects/osps/ocpi.osp.avnet  
    $ ocpidev create hdl assembly fsk_modem  
2. Copy fsk_modem XML from assets/hdl/assemblies/fsk_modem into the newly created HDL assembly   
    $ cd /home/user/projects/osps/ocpi.osp.avnet  
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/fsk_modem.xml /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem  
3. Copy fsk_modem Makefile from assets/hdl/assemblies/fsk_modem into the newly created HDL assembly   
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/Makefile /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem  
4. $ cd /home/user/opencpi/projects/ocpi.osp.avnet/hdl/assemblies/fsk_modem  
5. Copy the existing assets/hdl/assemblies/fsk_modem/cnt_zed_fmcomms_2_3_scdcd.xml and rename it cnt_microzed_\*\_fmcomms_2_3_scdcd.xml. Once it is copied and renamed edit the file to now include the platform name rather than the pre-existing zed name so that it points to the platform and the platforms new constaint file name cnt_microzed_\*\_fmcomms_2_3_scdcd.xdc  
6. Copy the existing constraints file of the MicroZed 7020 HDL platform  
    1. Edit the constraint file for the MicroZed 7020 platform to include the fmcomms2/3 contraints from the asset/hdl/assemblies/fsk_modem/cnt_zed_fmcomms_2_3_scdcd.xdc
    2. Edit the constraint file for the MicroZed 7020/7010 platform  
    The 7010 platform does not have IO Bank 13, so make sure to comment out the IOSTANDARD constraint for that bank:  
    - cnt_microzed_*_fmcomms_2_3_scdcd.xdc**
7. Edit the Makefile to include the container files for the desired MicroZed SOM   
**CRITICAL NOTE**  
**The provided "finalized" Makefile is configured to build only the MicroZed 7020 platform. The MicroZed 7010 platform is not included because it does not have enough resources to support the fsk_modem HDL assembly. See Appendix section [FSK Modem for 7010](#FSK-Modem-for-7010) for instructions on how to build a 7010 compatible assembly** 
8. Building the fsm_modem HDL Assembly
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/
    $ ocpidev build --hdl-assembly fsk_modem --hdl-platform microzed_20_cc

<a name="running-the-fsk_modem_app HDL Assembly"></a>

#### Running the fsk_modem_app HDL Assembly
1. Ensure the VADJ jumper on the Carrier Card is configured for 2V5.
2. Install the fmcomms2 into the FMC slot of the MZCC
3. Install a coax cable from the fmcomms2/TX1A to fmcomms2/RX2A
    - Yep it's TX1A-> RX2A.
4. Setup MZCC for Network Mode ([Network Mode setup](#network-mode-setup))  
    (i.e. customize your mynetsetup.sh to nfs mount to your dev host cdk and the various projects containing necessary run-time artifacts)
    - Be sure that the `mynetsetup.sh` script has the following mount points that will now include the ocpi.osp.avnet project
    ~~~
    mkdir -p /mnt/net                                                  
    mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory 
    mkdir -p /mnt/ocpi_core 
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
    mkdir -p /mnt/ocpi_assets                         
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
    mkdir -p /mnt/ocpi_assets_ts                                                                                       
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
    mkdir -p /mnt/ocpi_microzed
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/osps/ocpi.osp.avnet /mnt/ocpi_microzed
    ~~~
5. Change directories to the fsk_dig_radio_ctrlr application:  
    % cd /mnt/ocpi_assets/applications/fsk_dig_radio_ctrlr  
6. Setup artifacts search path:  
    % export OCPI_LIBRARY_PATH=/mnt/ocpi_microzed/hdl/assemblies/fsk_modem:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts  
7. Run app for enough time, -t, so that the entire data file (picture) can flow through the app  
    % ocpirun -v -d -x -t 15 fsk_modem_app.xml  
8. On your develop host:
    - $ cd /home/user/opencpi/projects/assets/applications/fsk_dig_radio_ctrlr  
    - $ eog fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin  
    - Success is defined as displaying the expected picture
<br>

<a name="build-and-run-all-component-unit-tests-for-cards-&-slots"></a>

### Build and run all Component Unit Tests for Cards & Slots
---------------------------------------------------------------------------------------------------
GOAL: 
- The goal of this section is to clean/build and retest the Component Unit Tests that were built in the previous section. Success of this section is to perform all of the Component Unit Tests making sure that each of the tests "pass or fail" as they did in the previous section. Please refer to the [Component Unit Test result table](#component-unit-test-result-table) section to insure that testing behavior is consistent.

Once Cards & Slots have been implemented into the design and the FSK application has been successfully executed, the user must now regression test using the framework provided Component Unit Tests. This will gives the user absolute certainty that the Cards & Slots design implementations have not interfered with performance of the system.

1. Clean all Component Unit Test bitstreams which were built prior to including the slot support  
    $ make cleantest
2. Build Component Unit Tests (in parallel)
    1. Build the core unit tests. (Could take several hours)  
        Open a new terminal on your local machine  
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/core  
        $ ocpidev build test --hdl-platform microzed_20_cc
    2. Build the assets unit tests (Could take several hours)  
        Open a new terminal on your local machine  
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/assets  
        $ ocpidev build test --hdl-platform microzed_20_cc
    3. Build the assets_ts unit tests (Could take several hours)  
        Open a new terminal on your local machine  
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/assets_ts  
        $ ocpidev build test --hdl-platform microzed_20_cc
3. Run the Unit Tests (Sequentially) on the Development Host  
    1. Ensure the [Server Mode setup](#server-mode-setup) is enabled on the MZCC
    2. The following article explains the use of the export command below (pg. 108):
        https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf  
        **NOTE:** This section is very painstaking, you must traverse into each unit .test directory (and sub-directory for assets) and perform a runonly followed by a verify of each of the tests in order to verify the full-scope of the Component Unit Tests. Some of these tests will fail, a chart is provided below that outlines the expected outcome for each of these tests (as of v2.1.0), use it as a reference as you work through this section.
    3. Run the core component unit tests
        $ cd /home/user/opencpi/projects/core/components/<.test>  
        $ make runonly OnlyPlatfoms=microzed_20_cc  
        $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1  
    4. Run the assets component unit tests
        $ cd /home/user/opencpi/projects/assets/components/\<sub-directory>/<.test>  
        $ make runonly OnlyPlatfoms=microzed_20_cc  
        $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1  
    5. Run the assets_ts component unit tests
        $ cd /home/user/opencpi/projects/assets_ts/components/<.test>  
        $ make runonly OnlyPlatfoms=microzed_20_cc  
        $ make verify OnlyPlatforms=microzed_20_cc TestAccumulateErrors=1  
<br>

---------------------------------------------------------------------------------------------------
<a name="appendix"></a>

## **APPENDIX**
---------------------------------------------------------------------------------------------------
### Petalinux Build
---------------------------------------------------------------------------------------------------
GOALS:
- The following Petalinux reference guide describes the commands and build flow that will be utilized in this section. These steps will be revisted in three sections of this guide, and will allow consequent bitstreams to be "spot checked": https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug1144-petalinux-tools-reference-guide.pdf  
- Successful completion of this section is a bootable SD card image utilizing the Petalinux utility in conjunction with resources provided in each of the following sections:
    1. [Review of the vendor's Reference Design Package](#review-of-the-vendor's-reference-design-package)
    2. [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)
    3. [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)
    4. [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)

**CRITICAL NOTE: As you progress through the guide this section will be utilized to create a 'petalinux project' in 4 different directories, one for each of the sections mentioned above. This will give the user the flexability to move between sections if needed. These directories are provided in the [Preview of the concluding directory structure](#preview-of-the-concluding-directory-structure) section.** 

1. Source Petalinux 2019.2  
    $ source /opt/pkg/petalinux/2019.2/settings.sh
2. Move into the /home/user directory
    $ cd /home/user/
2. Create a petalinux project directory depending on section  
    **CRITICAL NOTE: Depending on which section you are referencing you will create a petalinux project directory accordingly:**  
    [Review of the vendor's Reference Design Package](#review-of-the-vendor's-reference-design-package)
    $ petalinux-create -t project --template zynq --name "plx_7020_vendor"
    [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)   
    $ petalinux-create -t project --template zynq --name "plx_7020_minimal"  
    [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)  
    $ petalinux-create -t project --template zynq --name "plx_7020_cp"  
    [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)  
    $ petalinux-create -t project --template zynq --name "plx_7020_dp"  
3. CD into the newly created petalinux project directory.
4. Import the Hardware Configuration that was exported from the Vivado project. This is the *.xsa file that was created during the  File → Export → Export Hardware step during any of the three given sections above.  
    $ petalinux-config --get-hw-description=../ps7_microzed_7020_\<section>
5. Once the /misc/config System Configuration GUI is present in the terminal, continue with the default settings and press 'e' (EXIT) and 'y' (YES).
    - If you are presented with: "ERROR: Failed to generate meta-plnx-generated layer", this can be fixed by increasing the 'max_user_watches' as follows:  
    $ sudo sysctl -n -w fs.inotify.max_user_watches=524288
6. Build the project  
    $ petalinux-build  
7. Once the build is complete, move into the images/linux
8. Package the BOOT.BIN image  
    $ petalinux-package --boot --fsbl --fpga --u-boot --force
    1. There should now be a BOOT.BIN in the images/linux/ directory  
    **CRITICAL NOTE:**  
    **The use of "–fpga" is not required if the desire is to not program the FPGA from u-boot. If you are using the ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) method the '--fpga' option should not be used, as it does not contain a constraint file. This is due to issues seen when using Server Mode. When a bitstream IS loaded and packed into the BOOT.BIN, it is not possible to setup Server Mode.**
9.  Place the BOOT.BIN and image.ub into a micro-SD card that is configured for FAT32 and has (1) partition. Be sure to move or remove the contents from within the micro-SD card to be sure that it is empty.  
    - Link to partition SD-card: https://ragnyll.gitlab.io/2018/05/22/format-a-sd-card-to-fat-32linux.html  
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/  
    unmount SD-Card from computer  
10. Install the SD card into the MicroZed, power on the MicroZed, and open a serial connection to observe the petalinux boot process.  
    $ sudo screen /dev/ttyUSB0 115200  
    plx_7020_minimal login: root  
    Password:  
    root@plx_7020_minimal:~  
<br> 

<a name="static-ip-address-setup"></a>

#### Static IP Address Setup

If your network is not setup for DHCP or you would rather use a Static-IP address for your embedded platform follow the steps below. This will allow you to establish a Static-IP address so that you do not have to manually reconfigure your ethernet connection upon each boot-cycle:

- This solution is outlined here:  
https://www.xilinx.com/support/answers/68614.html  
$ md5sum recipes-core.tar.gz  
b85347e66a345000ac9b9a4994525c02

1. The recipes-core.tar.gz and its contents are loacated within the ocpi.osp.avnet repository in the following directory:  
    - \<path>/ocpi.osp.avnet/guide/microzed_cc/Petalinux
2. CD into vivado project depending on section  
    **NOTE: Depending on which section you are referencing you will cd into ONE of the follow directories:**  
    $ cd ps7_microzed_7020_preset/   -->  [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)  
    $ cd ps7_microzed_7020_ocpi_cp/  -->  [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)  
    $ cd ps7_microzed_7020_ocpi_dp/  -->  [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)
2. Copy the recipes-core directory to petalinux_project/project-spec/meta-user
3. Edit the interfaces file located below to the desired specifications:  
    - petalinux_project/project-spec/meta-user/recipes-core/init-ifupdown/init-ifupdown-1.0/interfaces
4. Repeat Steps 4-10 in the [Petalinux Build](#petalinux-build) to implement the Static-IP address changes.  
5. To verify that the changes have been successfully implemented perform the following command once you have logged into the device:
    $ ip a
    ~~~
    1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
        link/ether 00:0a:35:00:1e:53 brd ff:ff:ff:ff:ff:ff
        inet 192.168.0.10/24 scope global eth0
           valid_lft forever preferred_lft forever
        inet6 fe80::20a:35ff:fe00:1e53/64 scope link 
           valid_lft forever preferred_lft forever
    3: sit0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN group default qlen 1000
        link/sit 0.0.0.0 brd 0.0.0.0
    ~~~
    - Notice that the eth0 inet of 192.168.0.10 was established.  
<br>

<a name="FSK filerw for 7010"></a>

### FSK filerw for 7010  
---------------------------------------------------------------------------------------------------
GOALS:  
- Optional verification that the Data-Plane for the Microzed 7010 has been implemented, by building the appropriate HDL assembly (fsk_filerw_10), and running the application (app_fsk_filerw) and verifying the output.     

The Picozed 7010 FPGA does not have enough DSP resources to build the standard fsk_filerw worker, so a modified worker is needed. The fir_real_sse in home/user/opencpi/projects/assets/hdl/assemblies/fsk_filerw/fsk_filerw.xml requires more multiplies than the 7010 has available. The Pluto SDR OSP can be found here: https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr. It uses the same zynq chip as the Picozed 7010 and will be relied upon in this section to build the HDL assembly (fsk_filerw_10).    

**CODE BLOCK:**  
**The "finalized" code block of various files mentioned in this section are locaed in the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/hdl/assemblies/fsk_filerw_10/**  

<a name="building-the-fsk_filerw_10-assembly"></a>  

#### Building the fsk_filerw_10 Assembly  

**The fsk_filerw_10 hdl assembly is located in the following directory:**  
- **ocpi.osp.avnet/hdl/assemblies/fsk_filerw_10**  

1. Clone the Pluto SDR release-2.1.0 branch  
    $ cd /home/user/Downloads  
    $ git clone -b release-2.1.0 git@gitlab.com:opencpi/osp/ocpi.osp.plutosdr.git  
2. Make an HDL assembly directory for the fsk_filerw_10  
    $ cd /home/user/opencpi/projects/ocpi.osp.avnet/  
    $ ocpidev create hdl assembly fsk_filerw_10   
3. $ cd /home/user/opencpi/projects/ocpi.osp.avnet/hdl/assemblies/fsk_filerw_10   
4. Copy the Pluto SDR fsk_filerw.xml from /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_filerw into the newly created fsk_filerw_10 HDL assembly and rename it fsk_filerw_10.xml. This assembly XML differs from the FOSS fsk_filerw XML that is located in the assets project. It uses the fir_real_sse_for_xilinx worker instead of the fir_real_sse worker. It also lowers the value of the STAGES properties for several workers.    
    1. $ cp /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_filerw/fsk_filerw.xml /home/user/opencpi/projects/ocpi.osp.avnet/hdl/assemblies/fsk_filerw_10/fsk_filerw_10.xml  
5. Copy the fsk_filerw Makefile from /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_filerw into the newly created fsk_filerw_10 HDL assembly   
   1. $ cp /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_filerw/Makefile /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_filerw_10   
6. Building the fsk_filerw_10 HDL Assembly   
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/  
    $ ocpidev build --hdl-assembly fsk_filerw_10 --hdl-platform microzed_10_cc  
    $ cd hdl/assemblies/fsk_filerw_10/container-fsk_filerw_10_microzed_10_cc_base/target-zynq    
7. Copy the fsk_filerw_10 bitstream into the CDK  
    $ cp fsk_filerw_10_microzed_10_cc_base.bitz /home/user/opencpi/cdk/microzed_10_cc/sdcard-xilinx19_2_aarch32/opencpi/artifacts    

<a name="Building the FSK_10 filerw Application"></a>  

#### Building the FSK_10 filerw Application  

1. Copy the FSK application from the plutosdr directory into the ocpi.osp.avnet/applications directory and rename it FSK_10 to designate that this application will be used for the microzed_10_cc platform.  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet  
    $ cp -rf /home/user/Downloads/ocpi.osp.plutosdr/applications/FSK /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/FSK_10  
2. Clean the newly created FSK_10 directory of files that are unneeded for this application  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/FSK_10  
    $ rm app_fsk_rx_plutosdr.xml app_fsk_tx_plutosdr.xml app_fsk_txrx_plutosdr.xml FSK.xml aciapps  
3. Change the name of the FSK.cxx file to FSK_10.cxx  
    $ mv FSK.cxx ./FSK_10.cxx  
4. Update the FSK_10.cxx to reflect modifications made to use this application for the microzed_10_cc
    1. Find and Replace all cases of FSK with FSK_10  
    2. Find and Replace all cases of plutosdr with microzed_10_cc  
    3. Find and Replace all cases of PLUTOSDR with MICROZED_10_CC  
5. Fix an old script file gen_rrcos_taps.py in the FSK_10/scripts and update it with the current gen_rrcos_taps.py script found in projects/assets  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/FSK_10/scripts  
    $ rm gen_rrcos_taps.py  
    $ cp /home/user/opencpi/projects/assets/applications/FSK/scripts/gen_rrcos_taps.py /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/FSK_10/scripts  
    $ chmod 755 gen_rrcos_taps.py
6. Edit the FSK_10 Makefile in /home/user/opencpi/projects/ocpi.osp.avnet/applications/FSK_10 to incorporate the fixes made to the old script file gen_rrcos_taps.py  
    **Remove / Comment Out:**
    ~~~
    RccOnlyPlatforms=adi_plutosdr0_31
    ~~~
    **FROM:**  
    ~~~
    python scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/tx_rrcos_taps.dat
    python scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/rx_rrcos_taps.dat
    ~~~
    **TO:**  
    ~~~ 
    ./scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/tx_rrcos_taps.dat
    ./scripts/gen_rrcos_taps.py $(numTaps) $(alpha) `echo "1/$(baudRate)" | bc -l` `echo "$(baudRate)*$(spb)" | bc -l` $(maxTap) idata/rx_rrcos_taps.dat
    ~~~
7. Build the FSK_10 application for xilinx19_2_aarch32
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/FSK_10
    $ ocpidev build --rcc-platform xilinx19_2_aarch32
7. Copy the FSK_10 application into the CDK
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet
    $ cp -rf applications/FSK_10 /home/user/opencpi/cdk/microzed_10_cc/sdcard-xilinx19_2_aarch32/opencpi/applications

<a name="Run the FSK filerw Application on the microzed_10_cc"></a>

#### Run the FSK_10 filerw Application on the microzed_10_cc

1. Implement missing .so files into the sd-card xilinx19_2_aarch32 artifacts directory  
    **NOTE: The following files are needed based off of the /home/user/opencpi/projects/assets/applications/FSK/app_fsk_filerw.xml file under RCC Components. The ocpi.core.file_read is already given but the ocpi.assets.dsp_comps.baudTracking and ocpi.assets.dsp_comps.real_digitizer components need to be supplemented. Check the /home/user/opencpi/cdk/picozed_30_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts to see what is included**    
    1. Copy the Baudtracking_simple.so into the SD-Card xilinx19_2_aarch32 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/Baudtracking_simple.rcc/target-xilinx19_2_aarch32  
        $ cp Baudtracking_simple.so /home/user/opencpi/cdk/microzed_10_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts/  
    2. Copy the real_digitizer.so into the SD-Card xilinx19_2_aarch32 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/real_digitizer.rcc/target-xilinx19_2_aarch32  
        $ cp real_digitizer.so /home/user/opencpi/cdk/microzed_10_cc/sdcard-xilinx19_2_aarch32/opencpi/xilinx19_2_aarch32/artifacts/  
4. Copy the newly implemented file onto the SD-Card
    $ cp -RLp /home/user/opencpi/cdk/microzed_10_cc/sdcard-xilinx19_2_aarch32/opencpi/ /run/media/\<user>/\<sd-card>/
    - Ignore the ``cp: cannot stat ‘opencpi/applications/FSK_10/scripts/plotAndFft.py’: No such file or directory`` message
5. Install SD into MicroZed and apply power
6. Connect to the Platform  
    \# sudo screen /dev/tty/USB0 115200  
    Petalinux Login:  
    plx_7010_dp login: root  
    Password: root 
7. Mount the "opencpi" directory
    ~~~
    root@plx_7010_dp:~/# mkdir opencpi  
    root@plx_7010_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_7010_dp:~/# cd opencpi/  
    root@plx_7010_dp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch32     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
8. Setup the embedded platform environment for Standalone Mode by sourcing the appropriate setup script, i.e. `mysetup.sh`  
    root@plx_7030_dp:~/opencpi# cp `default_mysetup.sh` `mysetup.sh`  
    root@plx_7030_dp:~/opencpi# source /home/root/opencpi/mysetup.sh  
    Notice the blue LED illuminates to indicate that the FPGA has been loaded with the testbias bitstream and the stdout reports valid container detection for the ARM and PL
9. Setup the library path to the artifacts  
    % cd /home/root/opencpi/applications/FSK_10  
    % export OCPI_LIBRARY_PATH=/home/root/opencpi/artifacts:/home/root/opencpi/xilinx19_2_aarch32/artifacts  
10. Run application: FSK_10 filerw  
    % ./target-xilinx19_2_aarch32/FSK_10 filerw  
    ~~~
    % ./target-xilinx19_2_aarch32/FSK filerw
    Application properties are found in XML file: app_fsk_filerw.xml
    App initialized.
    App started.
    Waiting for done signal from file_write.
    real_digitizer: sync pattern 0xFACE found
    App stopped/finished.
    Bytes to file : 8950
    TX FIR Real Peak       = 31014
    Phase to Amp Magnitude = 20000
    RP Cordic Magnitude    = 16805
    RX FIR Real Peak       = 15823
    Application complete
    ~~~
11. In order to see the output of the execution of this application the user needs to copy over the output file located in /run/media/mmcblk0p1/opencpi/applications/FSK/odata/out_app_fsk_filerw.bin over to their development host. In order to do this:
    1. Move the SD to the development host, or
    2. Implement an IP address for the target-platform and scp the out_app_fsk_filerw.bin to the development host.
        1. Implement IP-Address (If it was not setup in [Static IP Address Setup](#static-ip-address-setup))   
            % ifconfig eth0 down  
            % ifconfig eth0 add \<Create Valid ip-address> netmask 255.255.255.0  
            % ifconfig eth0 up  
        2. Secure Copy the output file out_app_fsk_filerw.bin that was produced  
            % cd /run/media/mmcblk0p1/opencpi/applications/FSK/odata  
            % scp out_app_fsk_filerw.bin \<host>@\<host-ip>:\<path>  
12. View the results of the output file      
    $ eog \<path>/out_app_fsk_filerw.bin  
    ![Oriole](./images/oriole.png)  
<br>

<a name="FSK Modem for 7010"></a>

### FSK Modem for 7010
---------------------------------------------------------------------------------------------------
GOALS:
- Verify the FMC slot defined in the HDL Platform Worker, by building the appropriate HDL assembly (fsk_modem), running the application (fsk_modem_app) and verifying the output file. In addition to building an HDL assembly, a fmcomms2 or fmcomms3 daugthercard must be install on the MZCC.

The Microzed 7010 FPGA does not have enough DSP resources to build the standard fir_real_sse worker, so a modified worker is needed. In addition to this, the existing drc_fmcomms_2_3 rcc worker does not explicitly specify the number of cordic stages needed for the complex mixer worker, so it's implementation defaults to 16 stages. Because the Microzed 7010 can not support 16 stages for the complex mixer, a new drc_fmcomms_2_3 rcc worker must be created as well. This work has been done for the Pluto SDR OSP which also has a 7010 zynq FPGA, so it will be heavily referenced in this guide. The Pluto SDR OSP can be found here: https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr

**CODE BLOCK:**  
**The "finalized" code block of various files mentioned in this section are located in the ocpi.osp.avnet repository:**  
- **ocpi.osp.avnet/hdl/assemblies/fsk_modem_10/** 

<a name="building-the-fsk_modem_10-assembly"></a>

### Building the fsk_modem_10 Assembly

**The fsk_modem hdl assembly is located in the following directory:**   
- ocpi.osp.avnet/hdl/assemblies/fsk_modem_10

Container XML and constraint files must be created to include the fmcomms2 daughtercard in the build of the fsk_modem HDL assembly. The Pluto SDR OSP and zed are used as references. 

1. Clone the Pluto SDR release-2.1.0 branch  
    $ cd /home/user/Downloads
    $ git clone -b release-2.1.0 git@gitlab.com:opencpi/osp/ocpi.osp.plutosdr.git
2. Make an HDL assembly directory   
    $ ocpidev create hdl assembly fsk_modem_10
3. $ cd /home/user/opencpi/projects/ocpi.osp.avnet/hdl/assemblies/fsk_modem_10
4. Copy the existing /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/cnt_zed_fmcomms_2_3_scdcd.xml into the newly created fsk_modem_10 HDL assembly and rename it cnt_microzed_10_cc_fmcomms_2_3_scdcd.xml.  
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/cnt_zed_fmcomms_2_3_scdcd.xml /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem_10/cnt_microzed_10_cc_fmcomms_2_3_scdcd.xml
    1. Once it is copied and renamed edit the file to now include the platform name rather than the pre-existing zed name so that it points to the platform and the platforms new constaint file name cnt_microzed_10_fmcomms_2_3_scdcd.xdc
        ~~~
        <HdlContainer config='base'
                      only='microzed_10_cc'
                      constraints='cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc'>
        ~~~
5. Copy the Pluto SDR fsk_modem.xml from /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_modem into the newly created fsk_modem_10 HDL assembly and rename it fsk_modem_10.xml. This assembly XML differs from the zed fsk_modem XML. It uses the fir_real_sse_for_xilinx worker instead of the fir_real_sse worker. It also lowers the value of the STAGES properties for several workers.  
    $ cp /home/user/Downloads/ocpi.osp.plutosdr/hdl/assemblies/fsk_modem/fsk_modem.xml /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem_10/fsk_modem_10.xml
    1. Edit the fsk_modem_10.xml file to match the connections made in the cnt_microzed_cc_fmcomms_2_3_scdcd.xml file. Fix this by changing **path** to **path0**:  
        ~~~
        <Connection Name="in_to_asm_tx_path0" External="consumer">
        ...
        ...
        ...
        <Connection Name="out_from_asm_rx_path0" External="producer">
        ~~~
6. Copy the existing /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_10_cc/microzed_10_cc.xdc into the newly created fsk_modem_10 HDL assembly and rename it cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc  
    $ cp /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/platforms/microzed_10_cc/microzed_10_cc.xdc /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem_10/cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc
    1. Edit the newly created cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc file to include the fmcomms2/3 contraints from the /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/cnt_zed_fmcomms_2_3_scdcd.xdc
        1. From the cnt_zed_fmcomms_2_3_scdcd.xdc file implement all **create_clock** and **create_generated_clock** code below the following line into the newly created cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc:
            ~~~
            # ----------------------------------------------------------------------------
            # Clock constraints - platform_ad9361_data_sub.hdl
            # ----------------------------------------------------------------------------
            ...
            ...
            ~~~
        1. From the cnt_zed_fmcomms_2_3_scdcd.xdc file implement all code below the following line into the newly created cnt_microzed_10_cc_fmcomms_2_3_scdcd.xdc:  
            ~~~
            # ----------------------------------------------------------------------------
            # IOSTANDARD constraints - platform_ad9361_config.hdl
            # ----------------------------------------------------------------------------
            ...
            ...
            ~~~
        2. Make sure that bank 34/35 IOSTANDARDs are set to 2.5V  
            ~~~
            # Set the bank voltage for IO Bank 34 to 1.8V by default.
            # set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 34]];
            set_property IOSTANDARD LVCMOS25 [get_ports -of_objects [get_iobanks 34]];
            # set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];
        
            # Set the bank voltage for IO Bank 35 to 1.8V by default.
            # set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 35]];
            set_property IOSTANDARD LVCMOS25 [get_ports -of_objects [get_iobanks 35]];
            #set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
            ~~~
        3. Comment out the additional bank 35 IOSTANDARD  
            ~~~
            # Set the bank voltage for IO Bank 35 to 2.5V by default.
            # set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 35]];
            # set_property IOSTANDARD LVCMOS25 [get_ports -of_objects [get_iobanks 35]];
            # set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
            ~~~
        4. The 7010 platform does not have IO Bank 13, so make sure to comment out the IOSTANDARD constraint for that bank:  
            ~~~
            # Note that the bank voltage for IO Bank 13 is fixed to 3.3V on ZedBoard. 
            # set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]];

            ~~~
7. Copy the fsk_modem Makefile from /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem into the newly created fsk_modem_10 HDL assembly   
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/Makefile /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/assemblies/fsk_modem_10 
    1. Edit the Makefile to only include the container files for the desired MicroZed SOM   
        ~~~
        Containers= \
                cnt_microzed_10_cc_fmcomms_2_3_scdcd \
        ~~~
9. Building the fsm_modem HDL Assembly  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/
    $ ocpidev build --hdl-assembly fsk_modem_10 --hdl-platform microzed_10_cc

<a name="creating-the-drc_7010-rcc-worker"></a>

### Creating The drc_7010 RCC Worker  

A new DRC worker must be created because the existing drc worker (assets/hdl/cards/drc_fmccomms_2_3.rcc) does not define the number of CORDIC_STAGES for the complex_mixer worker. This leads to a runtime mismatch between the application worker name for the complex_mixer and what's contained in the bitstream artifact file. 

1. $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/
2. Create a new worker library   
    $ ocpidev create library --hdl-library cards
3. Create the new rcc worker   
    $ ocpidev create worker drc_fmcomms_2_3_7010.rcc -S drc-spec --hdl-library cards 
4. Copy the drc_fmcomms_2_3.xml from /home/user/opencpi/projects/assets/hdl/cards/drc_fmcomms_2_3.rcc/ into the newly created drc_fmcomms_2_3_7010.rcc worker and rename it drc_fmcomms_2_3_7010.xml
    $ cp /home/user/opencpi/projects/assets/hdl/cards/drc_fmcomms_2_3.rcc/drc_fmcomms_2_3.xml /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/cards/drc_fmcomms_2_3_7010.rcc/drc_fmcomms_2_3_7010.xml
    1. Edit the drc_fmcomms_2_3_7010.xml file to include CORDIC_STAGES_p property, which implements 12 Cordic Stages that can be handled by the resources of the Zynq 7010:
        ~~~
        <Property Name='INPUT_DATA_WIDTH_p' Value='12'/>
        <Property Name="CORDIC_STAGES_p"    Value="12"/>
        <Property Name='mag'                Value='1024'/>
        ~~~ 
5. Copy the drc_fmcomms_2_3.cc rcc worker implementation into the newly created worker directory.    
    $ cp /home/user/opencpi/projects/assets/hdl/cards/drc_fmcomms_2_3.rcc/drc_fmcomms_2_3.cc /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/cards/drc_fmcomms_2_3_7010.rcc/drc_fmcomms_2_3_7010.cc
6. Replace all occurrences of fmcomms_2_3 with fmcomms_2_3_7010.
7. Copy the drc_fmcomms_2_3 rcc worker Makefile into the newly created directory.   
    $ cp /home/user/opencpi/projects/assets/hdl/cards/drc_fmcomms_2_3.rcc/Makefile /home/user/opencpi/projects/osps/ocpi.osp.avnet/hdl/cards/drc_fmcomms_2_3_7010.rcc/Makefile
8. Build the rcc worker for xilinx19_2_aarch32     
    $ ocpidev build --rcc-platform xilinx19_2_aarch32

<a name="creating-the-fsk_dig_radio_ctrlr_10-application"></a>

### Creating the fsk_dig_radio_ctrlr_10 application

A new fsk_dig_radio_ctrlr_10 application must be created because the existing dig_radio_ctrlr application (/home/user/opencpi/projects/assets/applications/fsk_dig_radio_ctrlr) is not tailored to run on the Zynq 7010 chip.

1. $ cd /home/user/opencpi/projects/osps/ocpi.osp.avnet/
2. Create a new fsk_dig_radio_ctrlr_10 application
    $ ocpidev create application fsk_dig_radio_ctrlr_10
3. Remove fsk_dig_radio_ctrlr_10.cc and fsk_dig_radio_ctrlr_10.xml generated file
    $ rm /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/fsk_dig_radio_ctrlr_10/fsk_dig_radio_ctrlr_10.cc
    $ rm /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/fsk_dig_radio_ctrlr_10/fsk_dig_radio_ctrlr_10.xml
4. Implement fsk_modem_app.xml from the /home/user/Downloads/ocpi.osp.plutosdr/applications/fsk_dig_radio_ctrlr/fsk_modem_app.xml into the newly created fsk_dig_radio_ctrlr_10 application and rename it fsk_modem_app_10.xml
    $ cp /home/user/Downloads/ocpi.osp.plutosdr/applications/fsk_dig_radio_ctrlr/fsk_modem_app.xml /homer/user/opencpi/projects/osps/ocpi.osp.avnet/applications/fsk_dig_radio_ctrlr_10/fsk_modem_app_10.xml
    1. Edit the fsk_modem_app_10.xml to point to the fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin
    ~~~
    <!-- ACI relies on this property being set here in the OAS -->
    <Property Name="fileName"       Value="fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin"/>
    ~~~
5. Implement the idata/ directory from the original fsk_dig_radio_ctrlr application into the newly created fsk_dig_radio_ctrlr_10 application directory
    $ cp -RLp /home/user/opencpi/projects/assets/applications/fsk_dig_radio_ctrlr/idata /home/user/opencpi/projects/osps/ocpi.osp.avnet/applications/fsk_dig_radio_ctrlr_10/

<a name="running-the-fsk_modem_10-app"></a

### Running the fsk_modem_10 app
1. Ensure the VADJ jumper on the Carrier Card is configured for 2V5.
2. Install the fmcomms2 into the FMC slot of the MZCC
3. Install a coax cable from the fmcomms2/TX1A to fmcomms2/RX2A
    - Yep it's TX1A-> RX2A.
4. Setup MZCC for Network Mode ([Network Mode setup](#network-mode-setup))  
    (i.e. customize your mynetsetup.sh to nfs mount to your dev host cdk and the various projects containing necessary run-time artifacts)
    - Be sure that the `mynetsetup.sh` script has the following mount points that will now include the ocpi.osp.avnet project
        ~~~
        mkdir -p /mnt/net                                                  
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory 
        mkdir -p /mnt/ocpi_core 
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        mkdir -p /mnt/ocpi_assets                         
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
        mkdir -p /mnt/ocpi_assets_ts                                                                                       
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
        mkdir -p /mnt/ocpi_microzed
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/osps/ocpi.osp.avnet /mnt/ocpi_microzed
        ~~~
5. Change directories to the fsk_dig_radio_ctrlr application:  
    % cd /mnt/ocpi_assets/applications/fsk_dig_radio_ctrlr_10 
6. Setup artifacts search path:  
    % export OCPI_LIBRARY_PATH=/mnt/ocpi_microzed/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
7. Run app for enough time, -t, so that the entire data file (picture) can flow through the app 
    **CRITICAL NOTE: Issues have been noticed when running the application more than once. For best results, reload the FPGA between application runs.**
    % ocpirun -v -d -x -t 15 fsk_modem_app_10.xml  
8. On your develop host:
    - $ cd /home/user/opencpi/projects/assets/applications/fsk_dig_radio_ctrlr  
    - $ eog fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin  
    - Success is defined as displaying the expected picture
<br> 

<a name="reference-design"></a>

### Reference Design
---------------------------------------------------------------------------------------------------
GOALS:
- Begin with the MicroZed 7020 Reference Design (https://www.element14.com/community/docs/DOC-95640/l/microzed), remove modules and ports which are not absolutely required for the PS to boot properly to identify the "minimal" PS core IP configuration that will support boot. This step is iterative, meaning that only a small change or removal should be made and then verified on the MicroZed to successfully boot the embedded operating system. 
- The output of this section defines the "minimal" configuration that is Generated, HDL Wrapped and Exported for use in the Petalinux project that is used to create an SD-Card linux image which sucessfully boots on the target board.
- Once these steps are confirmed, then signals and ports (clock, reset, master (CP), slave (DP)) that are required for OpenCPI can be included, but this is detailed in a later step.

1. Source Vivado 2019.2  
    $ source /opt/Xilinx/Vivado/2019.2/settings64.sh
2. Open the MicroZed reference design:  
    $ cd /home/user/vendor_reference_design_package/mz7020_fmccc_2019_2/hardware/MZ7020_FMCCC_2019_2  
    $ vivado MZ7020_FMCCC.xpr
3. Once open, Navigate to the IP Integrator and Select Open Block Design.
4. (Iteratively) Remove the following components from the Block Design:
    1. ALL (6) proc_sys_reset*
    2. clk_wiz_0
    3. ALL (4)  axi_* ports
    4. (3)  External Ports → pl_leds_4bits, pl_pbs_5bits, hdmi_i2c
    5. Remove the ps7_axi_periph
    6. Remove the xlconcat_0, xlconstant_0, and interrupt_concat
    7. Remove the IRQ_F2P[1:0] Input port off of the ZYNQ7 Processing System by double clicking the ZYNQ
        1. Navigate to Interrupts tab
        2. → Drop down the Fabric Interrupts
        3. → Drop down the PL-PS Interrupts Ports
        4. → Uncheck the IRQ_F2P[15:0] interrupt port
    8. Create a signal between M_AXI_GP0_ACLK → FCLK_CLK0
5. Once Block Design updates are complete,
    1. Select Address Editor tab, RMC Auto-Assign Address
    2. Select Diagram tab, RMC Validate Design
    3. Select Generate Bitstream
6. Once the bitstream has been generated, export the hardware by selecting:
    1. File → Export → Export Hardware... → Include Bitstream →  OK  
        **NOTE: Be sure that the file is name: MZ7020_FMCCC_wrapper**
7. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly, and the "unique identifier" pattern on the LEDs is observed.
8. The design should look as follows:  
    ![Reference Design BD](./images/reference_design_BD.png)  
<br>

<a name="network-mode-setup"></a>

### Network Mode setup
---------------------------------------------------------------------------------------------------
GOAL: 
- The goal of this section is to enable the user with the ability to setup the Network Mode on the MZCC. Success of this section is the ability to source the customized mynetsetup.sh script that enables the Network Mode and provided the ability to load bitstreams from the Development Host (Computer) to the Platform Host (MZCC).

1. Establish a serial connection from the Host to the MicroZed, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
2. Petalinux Login:  
    plx_7020_dp login:root  
    Password:root  
3. Mount the OpenCPI filesystem
    \# mkdir opencpi  
    \# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    \# cd opencpi/  
4. Edit the default_mynetsetup.sh  
    1. Implement edits to reach the Development host opencpi directory  
        \# cp default_mynetsetup.sh ./mynetsetup.sh  
        \# vi mynetsetup.sh  
        Under the section '# Mount the opencpi development system as an NFS server, onto /mnt/net ...' Add the following lines which are necessary for mount the core, assets, and assets_ts build-in projects.  
        ~~~
        mkdir -p /mnt/net                                  
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
        
        mkdir -p /mnt/ocpi_core               
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        
        mkdir -p /mnt/ocpi_assets   
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets

        mkdir -p /mnt/ocpi_assets_ts
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
        ~~~
    2. Implement edits to automatically setup IP-Address  
        **NOTE: These commands can be done each time at boot-up, or be implemented into the mynetsetup.sh, if these commands do not work for your implementation consider manual entry**  
        % vi mynetsetup.sh  
        Under the commented section #Uncomment this section and change the MAC address for an enviornment with Multiple...  
        Do not uncomment that code, simply add the following code after that code-block.  
        ~~~
        ifconfig eth0 down
        ifconfig eth0 add <Valid ip-address> netmask 255.255.255.0
        ifconfig eth0 up
        ~~~
5. Source the `mynetsetup.sh` script to enable opencpi network mode  
    \# cd opencpi/  
    \# source /home/root/opencpi/mynetsetup.sh \<work station ip address> /home/user/opencpi  
    Example:  
    source /home/root/opencpi/mynetsetup.sh 192.168.0.100 /home/user/opencpi  
    ~~~
    root@my_petalinux_project:~/opencpi# source /home/root/opencpi/mynetsetup.sh 192.168.0.100 /home/user/opencpi
    macb e000b000.ethernet eth0: link down
    IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
    An IP address was detected.
    My IP address is: 192.168.0.10, and my hostname is: my_petalinux_project
    macb e000b000.ethernet eth0: link up (1000/Full)
    IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
    Attempting to set time from time.nist.gov
    rdate: bad address 'time.nist.gov'
    ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
    Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
    Running login script. OCPI_CDK_DIR is now /mnt/net/cdk.
    Executing /home/root/.profile
    No reserved DMA memory found on the linux boot command line.
    opencpi: loading out-of-tree module taints kernel.
    NET: Registered protocol family 12
    Driver loaded successfully.
    OpenCPI ready for zynq.
    Loading bitstream
    Bitstream successfully loaded
    Discovering available containers...
    Available containers:
     #  Model Platform            OS     OS-Version  Arch     Name
     0  hdl   microzed_20_cc                                 PL:0
     1  rcc   xilinx19_2_aarch32  linux  19_2        aarch32  rcc0
    ~~~
<br>

<a name="test result tables"></a>

### Test result tables
---------------------------------------------------------------------------------------------------
**OpenCPI Version: v2.1.0**  
Date Tested: 04/14/2021
<br>

**TABLE KEY:**  
P  - Pass   
Case - Case Listed failed  
Excluded - Excluded due to issues seen within the framework
<br> 
B  - Component Unit Tests Built successfully (7010 only)   
F  - Component Unit Tests Failed to build (7010 only)  
F* - Some Component Unit Test cases Failed to build (7010 only)  
P* - The Component Unit Tests that built, passed 
<br>

<a name="Component Unit Test Result table"></a>

#### **Component Unit Test result table**

| Component Library                 | zed           | microzed_20_cc | microzed_10_cc |
|-----------------------------------|---------------|----------------|----------------|
| **core/components**               |               |                |                |
| backpressure                      | P             | P              | P              |
| bias                              | P             | P              | P              |
| metadata_stressor                 | P             | P              | P              |
| **assets/components/comms_comps** |               |                |                |
| mfsk_mapper                       | P             | P              | P              |
| **assets/components/dsp_comps**   |               |                |                |
| cic_dec                           | P             | P              | P              |
| cic_int                           | case01.05,    | case01.05,     | F* P*          |
|                                   | case01.10     | case01.10      | case01.10      |
| complex_mixer                     | P             | P              | P              |
| dc_offset_filter                  | P             | P              | P              |
| downsample_complex                | P             | P              | P              |
| fir_complex_sse                   | P             | P              | F              |
| fir_real_sse                      | P             | P              | F* P*          |
| iq_imbalance_fixer                | P             | P              | P              |
| phase_to_amp_cordic               | P             | P              | P              |
| pr_cordic                         | P             | P              | P              |
| rp_cordic                         | P             | P              | P              |
| **assets/components/misc_comps**  |               |                |                |
| cdc_bits_tester                   | Excluded      | Excluded       |  Excluded      |
| cdc_count_up_tester               | Excluded      | Excluded       |  Excluded      |
| cdc_fifo_tester                   | Excluded      | Excluded       |  Excluded      |
| cdc_pulse_tester                  | Excluded      | Excluded       |  Excluded      |
| cdc_single_bit_tester             | Excluded      | Excluded       |  Excluded      |
| cswm_to_iqstream                  | P             | P              | P              |
| data_src                          | P             | P              | P              |
| delay                             | P             | P              | P              |
| iqstream_to_cswm                  |               | P              | P              |
| iqstream_to_timeiq                | P             | P              | P              |
| test_clock_generator              |               |                |                |
| timeiq_to_iqstream                | P             | P              | P              |
| **assets/components/util_comps**  |               |                |                |
| agc_real                          | P             | P              | P              |
| fifo                              | P             | P              | F              |
| pattern_v2                        | P             | P              | P              |
| test_tx_event                     | P             | P              | P              |
| timestamper                       | P             | P              | P              |
| timestamper_scdcd                 | P             | P              | P              |
| zero_pad                          | P             | P              | P              |
| **assets_ts/components**          |               |                |                |
| cic_dec_ts                        | P             | P              | P              |
| complex_mixer_ts                  | Excluded      | Excluded       | P              |
| dc_offset_filter_ts               | P             | P              | P              |
| downsample_complex_ts             | P             | P              | P              |
| fir_complex_sse_ts                | P             | P              | F* P*          |  
<br>

<a name="Verification test result table"></a>

#### **Verification test result table**

| Verification test                 | zed           | microzed_20_cc | microzed_10_cc |
|-----------------------------------|---------------|----------------|----------------|
| pattern_capture_asm               | P             | P              | P              |
| testbias                          | P             | P              | P              |
| filw_rw                           | P             | P              | P              |
| filw_txrx                         | P             | P              | P              |
<br>

<a name="fms-daughter-board-connection-table"></a>

### **FMC Daughter Board Connection Table**  
---------------------------------------------------------------------------------------------------
<br>
The table below indicates that there is no signal flow descrepancies between FPGA iterations (7020, 7010) as it pertains to the implementation of the FMC-HEADER when compared to the Zedboard. This means that there should be no difference when implementing the cards and slots of the MZCC between either the Zedboard or the different variations of the MicroZed. The only outstanding difference is the netlist of the FPGA fabric as indicated below.  
<br>  
<br> 

| FMC-HEADER          | ZEDBOARD        | FMC-HEADER / Pin    | CARRIER-CARD | MICROZED-7020/10| 20/10 |
|---------------------|-----------------|---------------------|--------------|-----------------|-------|
| FMC-LA06_P    (C10) | L21             | LA06_P        (C10) | 36 (JX1)     | JX1_LVDS_9_P    |  V15  |
| FMC-LA06_N    (C11) | L22             | LA06_N        (C11) | 38 (JX1)     | JX1_LVDS_9_N    |  W15  |
|                     |                 |                     |              |                 |       |
| FMC-LA10_P    (C14) | R19             | LA10_P        (C14) | 61 (JX1)     | JX1_LVDS_16_P   |  Y18  |
| FMC-LA10_N    (C15) | T19             | LA10_N        (C15) | 63 (JX1)     | JX1_LVDS_16_N   |  Y19  |
|                     |                 |                     |              |                 |       |
| FMC-LA14_P    (C18) | K19             | LA14_P        (C18) | 73 (JX1)     | JX1_LVDS_20_P   |  V17  |
| FMC-LA14_N    (C19) | K20             | LA14_N        (C19) | 75 (JX1)     | JX1_LVDS_20_N   |  V18  |
|                     |                 |                     |              |                 |       |
| FMC-LA18_CC_P (C22) | D20             | LA18_CC_P     (C22) | 54 (JX2)     | JX2_LVDS_13_P   |  J18  |
| FMC-LA18_CC_N (C23) | C20             | LA18_CC_N     (C23) | 56 (JX2)     | JX2_LVDS_13_N   |  H18  |
|                     |                 |                     |              |                 |       |
| FMC-LA27_P    (C26) | E21             | LA27_P        (C26) | 61 (JX2)     | JX2_LVDS_14_P   |  G17  |
| FMC-LA27_N    (C27) | D21             | LA27_N        (C27) | 63 (JX2)     | JX2_LVDS_14_N   |  G18  |
|                     |                 |                     |              |                 |       |
| FMC-SCL       (C30) | ---             | FMC_SCL       ---   | 84 (JX1)     | JX1_LVDS_23_P   |  P15  |
| FMC-SDA       (C31) | ---             | FMC_SDA       ---   | 82 (JX1)     | JX1_LVDS_23_N   |  P16  |
|                     |                 |                     |              |                 |       |
| FMC-LA01_CC_P (D8)  | N19             | LA01_CC_P     (D8)  | 48 (JX1)     | JX1_LVDS_13_P   |  N20  |
| FMC-LA01_CC_N (D9)  | N20             | LA01_CC_N     (D9)  | 50 (JX1)     | JX1_LVDS_13_N   |  P20  |
|                     |                 |                     |              |                 |       |
| FMC-LA05_P    (D11) | J18             | LA05_P        (D11) | 35 (JX1)     | JX1_LVDS_8_P    |  T16  |
| FMC-LA05_N    (D12) | K18             | LA05_N        (D12) | 37 (JX1)     | JX1_LVDS_8_N    |  U17  |
|                     |                 |                     |              |                 |       |
| FMC-LA09_P    (D14) | R20             | LA09_P        (D14) | 54 (JX1)     | JX1_LVDS_15_P   |  V20  |
| FMC-LA09_N    (D15) | R21             | LA09_N        (D15) | 56 (JX1)     | JX1_LVDS_15_N   |  W20  |
|                     |                 |                     |              |                 |       |
| FMC-LA13_P    (D17) | L17             | LA13_P        (D17) | 68 (JX1)     | JX1_LVDS_19_P   |  T17  |
| FMC-LA13_N    (D18) | M17             | LA13_N        (D18) | 70 (JX1)     | JX1_LVDS_19_N   |  R17  |
|                     |                 |                     |              |                 |       |
| FMC-LA17_CC_P (D20) | B19             | LA17_CC_P     (D20) | 53 (JX2)     | JX2_LVDS_12_P   |  H16  |
| FMC-LA17_CC_N (D21) | B20             | LA17_CC_N     (D21) | 55 (JX2)     | JX2_LVDS_12_N   |  H17  |
|                     |                 |                     |              |                 |       |
| FMC-LA23_P    (D23) | E15             | LA23_P        (D23) | 35 (JX2)     | JX2_LVDS_6_P    |  L19  |
| FMC-LA23_N    (D24) | D15             | LA23_N        (D24) | 37 (JX2)     | JX2_LVDS_6_N    |  L20  |
|                     |                 |                     |              |                 |       |
| FMC-LA26_P    (D26) | F18             | LA26_P        (D26) | 42 (JX2)     | JX2_LVDS_9_P    |  K19  |
| FMC-LA26_N    (D27) | E18             | LA26_N        (D27) | 44 (JX2)     | JX2_LVDS_9_N    |  J19  |
|                     |                 |                     |              |                 |       |
| FMC-TCK       (D29) | ---             | FMC-TCK       (D29) | ---          | ---             |  ---  |
| FMC-TDI       (D30) | ---             | FMC-TDI       (D30) | ---          | ---             |  ---  |
| FMC-TDO       (D31) | ---             | FMC-TDO       (D31) | ---          | ---             |  ---  |
| FMC-TMS       (D33) | ---             | FMC-TMS       (D33) | ---          | ---             |  ---  |
|                     |                 |                     |              |                 |       |
| FMC-CLK1_P    (G2)  | D18             | CLK1_M2C_P    (G2)  | 48 (JX2)     | JX2_LVDS_11_P   |  K17  |
| FMC-CLK1_N    (G3)  | C19             | CLK1_M2C_N    (G3)  | 50 (JX2)     | JX2_LVDS_11_N   |  K18  |
|                     |                 |                     |              |                 |       |
| FMC-LA00_CC_P (G6)  | M19             | LA00_CC_P     (G6)  | 47 (JX1)     | JX1_LVDS_12_P   |  N18  |
| FMC-LA00_CC_N (G7)  | M20             | LA00_CC_N     (G7)  | 49 (JX1)     | JX1_LVDS_12_N   |  P19  |
|                     |                 |                     |              |                 |       |
| FMC-LA03_P    (G9)  | N22             | LA03_P        (G9)  | 29 (JX1)     | JX1_LVDS_6_P    |  Y16  |
| FMC-LA03_N    (G10) | P22             | LA03_N        (G10) | 31 (JX1)     | JX1_LVDS_6_N    |  Y17  |
|                     |                 |                     |              |                 |       |
| FMC-LA08_P    (G12) | J21             | LA08_P        (G12) | 53 (JX1)     | JX1_LVDS_14_P   |  T20  |
| FMC-LA08_N    (G13) | J22             | LA08_N        (G13) | 55 (JX1)     | JX1_LVDS_14_N   |  U20  |
|                     |                 |                     |              |                 |       |
| FMC-LA12_P    (G15) | P20             | LA12_P        (G15) | 67 (JX1)     | JX1_LVDS_18_P   |  R16  |
| FMC-LA12_N    (G16) | P21             | LA12_N        (G16) | 69 (JX1)     | JX1_LVDS_18_N   |  R17  |
|                     |                 |                     |              |                 |       |
| FMC-LA16_P    (G18) | J20             | LA16_P        (G18) | 81 (JX1)     | JX1_LVDS_22_P   |  N17  |
| FMC-LA16_N    (G19) | K21             | LA16_N        (G19) | 83 (JX1)     | JX1_LVDS_22_N   |  P18  |
|                     |                 |                     |              |                 |       |
| FMC-LA20_P    (G21) | G20             | LA20_P        (G21) | 24 (JX2)     | JX2_LVDS_3_P    |  D19  |
| FMC-LA20_N    (G22) | G21             | LA20_N        (G22) | 26 (JX2)     | JX2_LVDS_3_N    |  D20  |
|                     |                 |                     |              |                 |       |
| FMC-LA22_P    (G24) | G19             | LA22_P        (G24) | 30 (JX2)     | JX2_LVDS_5_P    |  F16  |
| FMC-LA22_N    (G25) | F19             | LA22_N        (G25) | 32 (JX2)     | JX2_LVDS_5_N    |  F17  |
|                     |                 |                     |              |                 |       |
| FMC-LA25_P    (G27) | D22             | LA25_P        (G27) | 41 (JX2)     | JX2_LVDS_8_P    |  M17  |
| FMC-LA25_N    (G28) | C22             | LA25_N        (G28) | 43 (JX2)     | JX2_LVDS_8_N    |  M18  |
|                     |                 |                     |              |                 |       |
| FMC-LA29_P    (G30) | C17             | LA29_P        (G30) | 68 (JX2)     | JX2_LVDS_17_P   |  J20  |
| FMC-LA29_N    (G31) | C18             | LA29_N        (G31) | 70 (JX2)     | JX2_LVDS_17_N   |  H20  |
|                     |                 |                     |              |                 |       |
| FMC-LA31_P    (G33) | B16             | LA31_P        (G33) | 74 (JX2)     | JX2_LVDS_19_P   |  H15  |
| FMC-LA31_N    (G34) | B17             | LA31_N        (G34) | 76 (JX2)     | JX2_LVDS_19_N   |  G15  |
|                     |                 |                     |              |                 |       |
| FMC-LA33_P    (G36) | B21             | LA33_P        (G36) | 82 (JX2)     | JX2_LVDS_21_P   |  L14  |
| FMC-LA33_N    (G37) | B22             | LA33_N        (G37) | 84 (JX2)     | JX2_LVDS_21_N   |  L15  |
|                     |                 |                     |              |                 |       |
| FMC-VREF      (H1)  | M16/P15/F17/H20 | VREF          (H1)  | ---          | ---             |  ---  |
| FMC-PRSNT     (H2)  | AB14            | FMC-PRSNT     (H2)  | 87 (JX2)     | FMC-PRSNT_L     |  M14  |
|                     |                 |                     |              |                 |       |
| FMC-CLK0_P    (H4)  | L18             | CLK0_M2C_P    (H4)  | 42 (JX1)     | JX1_LVDS_11_P   |  U18  |
| FMC-CLK0_N    (H5)  | L19             | CLK0_M2C_N    (H5)  | 44 (JX1)     | JX1_LVDS_11_N   |  U19  |
|                     |                 |                     |              |                 |       |
| FMC-LA02_P    (H7)  | P17             | LA02_P        (H7)  | 24 (JX1)     | JX1_LVDS_5_P    |  P14  |
| FMC-LA02_N    (H8)  | P18             | LA02_N        (H8)  | 26 (JX1)     | JX1_LVDS_5_N    |  R14  |
|                     |                 |                     |              |                 |       |
| FMC-LA04_P    (H10) | M21             | LA04_P        (H10) | 30 (JX1)     | JX1_LVDS_7_P    |  W14  |
| FMC-LA04_N    (H11) | M22             | LA04_N        (H11) | 32 (JX1)     | JX1_LVDS_7_N    |  Y14  |
|                     |                 |                     |              |                 |       |
| FMC-LA07_P    (H13) | T16             | LA07_P        (H13) | 41 (JX1)     | JX1_LVDS_10_P   |  U14  |
| FMC-LA07_N    (H14) | T17             | LA07_N        (H14) | 43 (JX1)     | JX1_LVDS_10_N   |  U15  |
|                     |                 |                     |              |                 |       |
| FMC-LA11_P    (H16) | N17             | LA11_P        (H16) | 62 (JX1)     | JX1_LVDS_17_P   |  V16  |
| FMC-LA11_N    (H17) | N18             | LA11_N        (H17) | 64 (JX1)     | JX1_LVDS_17_N   |  W16  |
|                     |                 |                     |              |                 |       |
| FMC-LA15_P    (H19) | J16             | LA15_P        (H19) | 74 (JX1)     | JX1_LVDS_21_P   |  W18  |
| FMC-LA15_N    (H20) | J17             | LA15_N        (H20) | 76 (JX1)     | JX1_LVDS_21_N   |  W19  |
|                     |                 |                     |              |                 |       |
| FMC-LA19_P    (H22) | G15             | LA19_P        (H22) | 18 (JX2)     | JX2_LVDS_1_P    |  B19  |
| FMC-LA19_N    (H23) | G16             | LA19_N        (H23) | 20 (JX2)     | JX2_LVDS_1_N    |  A20  |
|                     |                 |                     |              |                 |       |
| FMC-LA21_P    (H25) | E19             | LA21_P        (H25) | 29 (JX2)     | JX2_LVDS_4_P    |  E18  |
| FMC-LA21_N    (H26) | E20             | LA21_N        (H26) | 31 (JX2)     | JX2_LVDS_4_N    |  E19  |
|                     |                 |                     |              |                 |       |
| FMC-LA24_P    (H28) | A18             | LA24_P        (H28) | 36 (JX2)     | JX2_LVDS_7_P    |  M19  |
| FMC-LA24_N    (H29) | A19             | LA24_N        (H29) | 38 (JX2)     | JX2_LVDS_7_N    |  M20  |
|                     |                 |                     |              |                 |       |
| FMC-LA28_P    (H31) | A16             | LA28_P        (H31) | 62 (JX2)     | JX2_LVDS_15_P   |  F19  |
| FMC-LA28_N    (H32) | A17             | LA28_N        (H32) | 64 (JX2)     | JX2_LVDS_15_N   |  F20  |
|                     |                 |                     |              |                 |       |
| FMC-LA30_P    (H34) | C15             | LA30_P        (H34) | 73 (JX2)     | JX2_LVDS_18_P   |  K14  |
| FMC-LA30_N    (H35) | B15             | LA30_N        (H35) | 75 (JX2)     | JX2_LVDS_18_N   |  J14  |
|                     |                 |                     |              |                 |       |
| FMC-LA32_P    (H37) | A21             | LA32_P        (H37) | 81 (JX2)     | JX2_LVDS_20_N   |  N15  |
| FMC-LA32_N    (H38) | A22             | LA32_N        (H38) | 83 (JX2)     | JX2_LVDS_20_N   |  N16  |
| ---                 | ---             |                     |              |                 |       |
| ---                 | ---             | FMC_MGT_TX_P  (C2)  | 31 (JX3)     | MGTTX3_P        |  ---  |
| ---                 | ---             | FMC_MGT_TX_N  (C3)  | 33 (JX3)     | MGTTX3_N        |  ---  |
| ---                 | ---             |                     |              |                 |       |
| ---                 | ---             | FMC_MGT_RX_P  (C6)  | 26 (JX3)     | MGTRX3_P        |  ---  |
| ---                 | ---             | FMC_MGT_RX_N  (C7)  | 28 (JX3)     | MGTRX3_N        |  ---  |
| ---                 | ---             |                     |              |                 |       |
| ---                 | ---             | FMC_GBTCLK_P  (D4)  | ---          | ---             |  ---  |
| ---                 | ---             | FMC_GBTCLK_N  (D5)  | ---          | ---             |  ---  |
<br>
