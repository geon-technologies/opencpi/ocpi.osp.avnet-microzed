# avnet

## Provided 
---
The following is provided within this repo:  
- Microzed OSP's  
    - Microzed 7020 + CC   
    - Microzed 7010 + CC   
- OSP Development Guide using the Picozed 7020 + CC as a template  
- Code Blocks for each design stage of the Development Guide as a reference   

## Getting Started
---
See Microzed_Getting_Started_Guide.tex located in the doc/microzed_cc directory  

## Guide to developing an OSP for Zynq-7000 based platforms
---
See Guide.md located in the guide/microzed_cc/ directory  

## Version Tested against
---
This guide and subsequent OSP's were tested against and are compatable with the following OpenCPI version:  
v2.1.0 

Notes:
---
  - hdl/assemblies
      Contains copies of assemblies from the OpenCPI assets project, with the
      addition of MicroZed SOM specific container XML files. A desirable framework
      improvement would be to mitigate the need to copy assemblies from external
      projects in order to use containers made possible internally.

