\iffalse
This file is protected by Copyright. Please refer to the COPYRIGHT file
distributed with this source distribution.

This file is part of OpenCPI <http://www.opencpi.org>

OpenCPI is free software: you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
\fi

%----------------------------------------------------------------------------------------
% Required document specific properties
%----------------------------------------------------------------------------------------
\def\docTitle{MicroZed Getting Started Guide}
\def\snippetpath{./doc/}
%----------------------------------------------------------------------------------------
% Global latex header (this must be after document specific properties)
%----------------------------------------------------------------------------------------
\input{\snippetpath/LaTeX_Header}
\graphicspath{{figures/}}
%----------------------------------------------------------------------------------------
\usepackage[normalem]{ulem}
%----------------------------------------------------------------------------------------

\begin{document}
\maketitle
\thispagestyle{empty}
\newpage

\begin{center}
	\textit{\textbf{Revision History}}
	\begin{table}[H]
		\label{table:revisions} % Add "[H]" to force placement of table
		\begin{tabularx}{\textwidth}{|c|X|l|}
			\hline
			\rowcolor{blue}
			\textbf{Revision} & \textbf{Description of Change} & \textbf{Date} \\
			\hline
			- & Initial Release & 4/2021 \\
			\hline
		\end{tabularx}
	\end{table}
\end{center}
\newpage

\tableofcontents
\newpage

\section{References}
The reference(s) in Table 1 can be used as an overview of OpenCPI and may prove useful. The installation guide is required since many of the steps mentioned here are defined there, especially in the section: Enabling OpenCPI Development for Embedded Systems. This document provides details for this system that can be applied to procedures defined there.  It is best to use both documents at the same time. This document assumes a basic understanding of the Linux command line (or ``shell'') environment.  

\textbf{The MicroZed project contains the "Guide for developing an OpenCPI Board Support Package (OSP) - Case Study MicroZed 7020 and FMC Carrier Card" and should be referenced for additional setup and installation.}

\def\refcapbottom{}
\input{\snippetpath/References_Table_noRPM}
\section{Overview}
This document provides: steps for configuring a factory provided Avnet MicroZed SOM (7020, 7010) + Carrier Card with the OpenCPI run-time environment for executing applications.\medskip

The MicroZed SOM (7020, 7010) does \textbf{NOT} require the use Carrier Card and can be used, on its own, as a hardware accelerator. All testing applications that do not require daughter-card implementation, such as the FMCOMMs board, can be leveraged on the MicroZed SOM (7020, 7010). Included on the Stand-alone board is the Ethernet port, Micro-USB port, USB port, and the Micro-SD card slot. The MicroZed SOM (7020, 7010) is powered via it's Micro-USB port. Below is a picture of the MicroZed SOM (7020) without its Carrier-Card.
\begin{figure}[H]
    \caption{MicroZed SOM (7020) without the Carrier-Card}
	\centerline{\includegraphics[scale=0.05]{microzed_SOM}}
	\label{fig:microzed_uart}
\end{figure}
   

\section{Prerequisites}
\begin{flushleft}
It is assumed that the tasks defined in the ``Enabling OpenCPI Development for Embedded Systems'' section of the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf} has been successfully performed.
Support for the MicroZed system(s) is located in the \textit{ocpi.osp.avnet} project, targeting the \textit{microzed\_20\_cc, microzed\_10\_cc} OpenCPI HDL (FPGA) platforms and the \textit{xilinx19\_2\_aarch32} OpenCPI RCC (software) platform.  The RCC platforms are supported by the \textit{ocpi.core} built-in project.

\subsection{Bug Fix to the framework}

The following bug fix MUST be applied to the v2.1.0 framework release to address an intermittent run-time failure observed on the embedded system. The implementation of this bug fix is required PRIOR to installing (i.e. cross-compiling) the OpenCPI run-time utilities for any targeted embedded Software RCC Platform, such as, the xilinx19\_2\_aarch32.\medskip
\begin{verbatim}
$ git diff runtime/dataplane/transport/src/OcpiInputBuffer.cxx
WARNING: terminal is not fully functional
-  (press RETURN) 
diff --git a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx b/runtime/dataplane/transport
/src/OcpiInputBuffer.cxx
index 3d37c6e..03276cd 100644
--- a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
+++ b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
@@ -147,7 +147,7 @@ void InputBuffer::update(bool critical)
        sizeof(BufferMetaData)*MAX_PCONTRIBS);
 
     memset(m_bmdVaddr,0,sizeof(BufferMetaData)*MAX_PCONTRIBS);
-    getPort()->getEndPoint().doneWithInput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
+    getPort()->getEndPoint().doneWithOutput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
     m_sbMd = static_cast<volatile BufferMetaData (*)[MAX_PCONTRIBS]>(m_bmdVaddr);
   }
\end{verbatim}

\subsection{RCC Platforms}
The RCC platform \textit{xilinx19\_2\_aarch32} is required by each of the MicroZed variations.\medskip

For successful installation and deployment of xilinx19\_2\_aarch32 RCC platform, take special note in the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf} steps that outline the implementation of the ZynqReleases directory and git directory. The ZynqReleases directory and git directory will be place in the Xilinx tools installation directory (/tools/Xilinx or /opt/Xilinx).\medskip

As a convenience, a 2019.2-microzed\_**\_cc-release.tar.xz file is available in each of the platform worker directories for each variation of the MicroZed (7020, 7010). These files can be found in the ocpi.osp.avnet repository and can be placed into the ZynqReleases directory as a \textbf{Linux Prebuild Image}.\medskip

File Location:\\ /home/user/ocpi.osp.avnet/hdl/platform/microzed\_**\_cc/

\subsection{Vendor Software Setup}
Also indicated in the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf}, are the tools used for software cross-compilation from the Xilinx Vitis 2019.2, and the tools used for the FPGA are any recent version of the Xilinx Vivado Webpack tools. The Linux Kernel is based on the Xilinx Linux kernel tagged \textit{xilinx\_v2019.2} (for \textit{xilinx19\_2\_aarch32}). The installation of these tools is described in the installation guide.\medskip

\subsection{Building Required Projects}
\label{sec:Building OpenCPI projects}
The standard built-in OpenCPI projects are built using the instructions in the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf}. This results in a single test executable application (\textit{testbias}) based on the \textit{testbias} HDL assembly (FPGA bitstream), which are both in the \textit{assets} built-in project.

\subsection{Setup and Build summary}
\label{sec:Setup and Build summary}
Below is an abbreviated set of steps that will guide the user through the installation and deployment of the microzed\_20\_cc hdl-platform and the xilinx19\_2\_aarch32 rcc-platform.\medskip

The steps provided below rely heavily on the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf}. For additional information, reference the "Guide for developing an OpenCPI Board Support Package (OSP) - Case Study MicroZed 7020 and FMC Carrier Card" at ocpi.osp.avnet/guide/microzed\_cc/Guide.md \medskip

The values of the "Duration" in the steps below are approximated and directly dependent on the system resources of the development host. They are roughly based on a development host having an Intel Xeon 3.90GHz x 16 with 128GB RAM.

\begin{verbatim}
1. Clone and Install the OpenCPI framework. (Duration 30 min)
    $ cd /home/user
    $ git clone https://gitlab.com/opencpi/opencpi.git
    $ cd opencpi
    $ git checkout tags/v2.1.0
    $ ./scripts/install-opencpi.sh  
    
2. Install for the zed HDL Platform (Duration < 6 hours)
This step is a workaround for an issue in the framework, where the framework does
not properly install an HDL Platform that is located in the projects/osp/.
Therefore, the user is instructed to install for a built-in HDL platform that
targets the same Xilinx device family. This will ensure that all of the HDL assets
are built for the correct target device. Once all of the HDL assets are built,
then the target MicroZed HDL platform can be built, and finally the MicroZed
platform can be installed and deployed.

    $ ocpiadmin install platform zed

3. Clone ocpi.osp.avnet project into the appropriate directory
    $ cd /home/user/opencpi/projects/osps
    $ git clone <location>/ocpi.osp.avnet.git

4. Register the ocpi.osp.avnet project
    $ cd ocpi.osp.avnet/
    $ ocpidev register project

5. Build for desired target MicroZed SOM (Duration < 5 min)
    $ ocpidev build --hdl-platform microzed_20_cc

6. Install HDL platform (Duration < 20 min)
    $ cd /home/user/opencpi
    $ ocpiadmin install platform microzed_20_cc

7. Using the steps described in the OpenCPI Installation Guide or
ocpi.osp.avnet/guide/microzed_cc/Guide.md, setup the ZynqReleases directory
(/tools/Xilinx/ZynqReleases/ or /opt/Xilinx/ZynqReleases/). Copy the desired MicroZed variation
2019-microzed_**_cc-release.tar.xz file that contains the BOOT.bin and image.ub
located in each of the MicroZed platform-worker directories shown below, in to the ZynqReleases.
Pay close attention to setting the permissions on the created directory and its contents.
    /home/users/opencpi/projects/osps/ocpi.osp.avnet/hdl/platform/microzed_**_cc/

8. Using the steps described in the OpenCPI Installation Guide, setup the git directory
(/tools/Xilinx/git or /opt/Xilinx/git) for cross-compiling the OpenCPI run-time tools for
xilinx19_2_aarch32.
Pay close attention to setting the permissions on the created directory and its contents.

9. Install RCC platform (Duration < 10 min)
    $ ocpiadmin install platform xilinx19_2_aarch32				

10. Deploy HDL platform with RCC platform
    $ ocpiadmin deploy platform xilinx19_2_aarch32 microzed_20_cc
\end{verbatim}

\pagebreak

\subsection{Hardware Setup}
\begin{itemize}
\item \textbf{Avnet MicroZeds Product Package}\\ \medskip
It is expected that this system includes a power supply, micro-USB to USB cable, micro-USB to female-USB adapter and a micro-SD card (16GB). \\ \medskip

\item \textbf{UART / SD}:
The micro-USB serial port is located on the top-side of the MZCC, and can be used to access the serial connection with the process. The micro-SD slot is right below it. Both will be used throughout this guide.
\begin{figure}[H]
    \caption{Serial USB, and micro-SD card}
	\centerline{\includegraphics[scale=0.05]{microzed_uart_sd}}
	\label{fig:microzed_uart}
\end{figure}

\item \textbf{FMC / SD}:
The MicroZeds LPC-FMC Slot.
\begin{figure}[H]
	\caption{MicroZeds FMC Slot}
	\centerline{\includegraphics[scale=0.05]{microzed_fmc}}
	\label{fig:microzed_fmc_sd}
\end{figure}

\item \textbf{Ethernet cable}:
An Ethernet port is available on the MicroZed and is required when the Network or Server Mode is used. The OpenCPI BSP for the MicroZeds are configured for DHCP.
\begin{figure}[H]
	\caption{Connected Ethernet}
	\centerline{\includegraphics[scale=0.05]{microzed_ether}}
	\label{fig:microzed_ether}
\end{figure}

\item \textbf{OpenCPI MZCC supported daughtercards (OPTIONAL)}\\
The MZCC has a FMC LPC slot that is used to connect plug-in modules or daughtercards. Currently, OpenCPI supports two FMC daughtercards, which may be installed on the MZCC:
\begin{itemize}
	\item Analog Devices FMCOMMS2
	\item Analog Devices FMCOMMS3
\end{itemize} 
\begin{figure}[H]
	\caption{Connected FMC3 Daughter-Card}
	\centerline{\includegraphics[scale=0.05]{microzed_fmc}}
	\label{fig:microzed_ether}
\end{figure}

\item \textbf{Access to a network which supports DHCP. (Network or Server Mode)}

\item \textbf{SD card reader}
\end{itemize}
\end{flushleft}

\newpage

\section{SD Card Setup}
\label{sec:SD_Card_Setup}
The installation guide provides the procedure for creating a new SD-card for OpenCPI and customizing some of the files for your particular configuration. The usual way is to make a raw copy of the manufacturer supplied card to a new card, preserving formatting and content, and then removing most original files and copying files from OpenCPI. If you need to format the SD card for this system, it should be a single FAT32 partition.\medskip

\noindent Once the microzed\_20\_cc (hdl-platform) and Xilinx19\_2\_aarch32 (rcc-platform) have been successfully installed and deployed in the \nameref{sec:Setup and Build summary}, the following steps can be taken in order to create a valid SD-Card for the MicroZed system.
\begin{verbatim}
1. Create SD (single FAT32 partition) for installing in the MicroZed Carrier Card (MZCC)

2. cp -RLp cdk/microzed_20_cc/sdcard-xilinx19_2_aarch32/* /run/media/<user>/<card>/
\end{verbatim}

\subsection{Multiple MZCC on the same network}
\label{sec:Multiple MZCC on the same network}
If it is required that multiple MZCC are to be on the same network, the following change to the embedded device startup scripts is required. This is necessary because by default the MZCC have the same MAC address from the PetaLinux build. To resolve this, uncomment the following lines in the mynetsetup.sh and/or mysetup.sh scripts and modify the Ethernet address to be unique:
\begin{verbatim}
    # ifconfig eth0 down
    # ifconfig eth0 hw ether <unique MAC address> # e.g. ifconfig eth0 hw ether 00:0a:35:00:01:24
    # ifconfig eth0 up
    # udhcpc
\end{verbatim}

\pagebreak

\section{Hardware Setup}

\subsection{Establish a Serial Connection}
The installation guide provides the procedure for establishing a serial console.  The MZCC console serial port is configured for 115200 baud. The cable used is a micro-USB to USB-A cable to connect its console micro-USB port to the development host.

\subsection{Booting the MZCC from the SD card}
\begin{enumerate}
\item Remove power from the MZCC.
\item Ensure jumpers are configured correctly, below is a top-down view of the MicroZed SOM and MicroZed CC and their respective jumpers (highlighted in blue). Consult the user-guide of the MicroZed SOM and MZCC for further information.\medskip

\begin{figure}[ht]
	\caption{Top View of the MicroZed}
	\centerline{\includegraphics[scale=0.08]{microzed_top}}
	\label{fig:microzed_top}
\end{figure}
\begin{enumerate}

\newpage

\item \textbf{MicroZed SOM}: Default the jumpers are set to boot from QSPI, in order to boot from a micro-SD card the jumpers need to be set to the orientation in the image below.
\begin{figure}[ht]
	\caption{Top View of the MicroZed SOM SD-card boot jumper setting}
	\centerline{\includegraphics[scale=0.05]{microzed_som_boot_jmp}}
	\label{fig:microzed_top}
\end{figure}

\item \textbf{MicroZed CC}: The VADJ jumpers of the MicroZed CC in the image shown below are set to 2.5 V.
\begin{figure}[ht]
	\caption{Top View of the MZCC Vadj jumper setting for the FMC-LPC}
	\centerline{\includegraphics[scale=0.05]{microzed_CC_jmp}}
	\label{fig:microzed_top}
\end{figure}
\end{enumerate}
\item With the contents provided in the \nameref{sec:SD_Card_Setup} , insert the micro-SD card into the micro-SD card slot.
\item Connect a terminal to the micro-USB connector labeled 'UART' on the MZCC. The baud rate is 115200 baud.
\item Start the terminal emulator on the development host (usually the \textit{screen} command) at 115200 baud.
\item Apply power to the MZCC with the terminal still connected.
\end{enumerate}
\pagebreak

\section{Configuring the run-time environment on the platform}
The installation guide provides the procedure for setting up and verifying the run-time environment.
This system is initially set with  ``\textbf{root}'' for user name and password.

\begin{flushleft}
After a successful boot to PetaLinux, login to the system, using  ``\textbf{root}`` for user name and password.\medskip

Take note of the \textbf{root@plx\_7020\_dp} indicating that the Control-Plane and Data-Plane of the MicroZed 7020 have successfully booted using PetaLinux.

\begin{figure}[H]
	\centerline{\includegraphics[scale=0.5]{microzed_boot}}
	\caption{Successful Boot to PetaLinux}
	\label{fig:boot1}
\end{figure}
\medskip

Boot issues were observed when testing on the MicroZed REV F where the following command had to be implemented to get the device to successfully boot:\medskip
\begin{verbatim}
Zynq> setenv default_bootcmd run uenvboot; run cp_kernel2ram && bootm ${netstart}
\end{verbatim}
\medskip
This issue was not observed when testing on the MicroZed REV G.
\medskip
The REV numbers can be found underneath the MicroZed SOMs nearest the PMOD connector and are incorporated within the copper of the PCB. Image below.
\begin{figure}[H]
	\centerline{\includegraphics[scale=0.05]{microzed_REV}}
	\caption{The MicroZed SOM (7020) REV G Number}
	\label{fig:boot1}
\end{figure}
\pagebreak

\subsection{Server Mode Setup}
\textbf{CRITICAL NOTE: These instructions for using Server Mode are not officially published, use them with caution.\medskip}

The user may also use Network Mode for running the \textit{testbias} application. Instructions for using Network Mode can be found in the APPENDIX section of this guide and in the appendix of the ocpi.osp.avnet/guide/microzed\_cc/Guide.md.\medskip

\label{sec:Server_Mode_Setup}
\textbf{Server-side setup:}
\begin{verbatim}
1. Establish a serial connection from the Host to the MicroZed, open a terminal window:
    $ sudo screen /dev/ttyUSB0 115200
    
2. Petalinux Login:
    plx_7020_dp login:root
    Password:root
     
3. Network setup:
    $ ifconfig eth0 down
    $ ifconfig eth0 add <Vailid remote ip-address> netmask 255.255.255.0
    $ ifconfig eth0
\end{verbatim}

\textbf{Client-side setup:}
\begin{verbatim}
1. Change to your Opencpi directory:
    $ cd /home/user/opencpi
    
2. Export some Opencpi enviornment variables to discover the remote server:
    $ export OCPI_SERVER_ADDRESSES=<Valid ip-address>:<Valid port>
    $ export OCPI_SOCKET_INTERFACE=<Valid socket>
    - Item "Valid socket" is the name of the Ethernet interface of the development host
    
3. Load "sandbox" onto the server:
    $ ocpiremote load -s xilinx19_2_aarch32 -w microzed_20_cc
    
4. Start the Server-Mode with the DMA_CACHE_MODE disabled and load the default bitstream (testbias):
    $ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0

Troubleshooting:

    If you are presented with the following error, the user may need to disable 
    or reconfigure the firewall of the host system:
    
    OCPI( 2:991.0828): Exception during application shutdown: error reading from container 
    server "": EOF on socket read Exiting for exception: error reading from container 
    server "": EOF on socket read
    
    You can disable your firewall until it is rebooted with the two following commands:  
    $ sudo systemctl disable firewalld  
    $ sudo systemctl mask --now firewalld      
\end{verbatim}
\end{flushleft}
\pagebreak

\section{Run an Application}
With \nameref{sec:Server_Mode_Setup} now enabled, the following steps will run the test application \textit{testbias}, based on the \textit{testbias} HDL assembly, both in the \textit{assets} built-in project.

\begin{verbatim}
1. On the Client host, navigate to the OpenCPI applications located in the assets built-in project
    $ cd /home/user/opencpi/projects/assets/applications

2. Setup the OCPI_LIBRARY_PATH
    $ export OCPI_LIBRARY_PATH=/home/user/opencpi/projects/assets/hdl/assemblies/testbias:
    /home/user/opencpi/projects/core/artifacts:/home/user/opencpi/projects/assets/artifacts/

3. Run the testbias application executing file_read and file_write on the host
    $ ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 -P file_read=centos7 -P file_write=centos7
    testbias.xml

4. Validate the output data by comparing to the input data, when the biasvalue is 0
    $ md5sum test.input
    $ md5sum test.output
\end{verbatim}

\pagebreak

\begin{appendices}
% Bring in kernel message snippet
\section{Driver Notes}
\input{\snippetpath/Driver_Snippet}

\section{Network Mode}
The testbias application can also be tested using Network Mode described here:
\begin{verbatim}
1. Establish a serial connection from the Host to the microzed, open a terminal window:
    $ sudo screen /dev/ttyUSB0 115200
    
2. Petalinux Login:
    plx_7020_dp login:root
    Password:root
     
3. Network setup:
    $ ifconfig eth0 down
    $ ifconfig eth0 add <Vailid remote ip-address> netmask 255.255.255.0
    $ ifconfig eth0 up
    
4. Mount opencpi
    $ mkdir opencpi
    $ mount /run/media/mmcblk0p1/opencpi opencpi/
    
5. Rename and edit the default_mynetsetup.sh
\end{verbatim}

Refer to the \githubio[\textit{OpenCPI Installation Guide}]{OpenCPI\_Installation\_Guide.pdf} on renaming and editing the default\_mynetsetup.sh file.

\begin{verbatim}
6. Source the mynetsetup.sh script
    $ source /run/media/mmcblk0p1/opencpi/mynetsetup.sh <Valid host ip-address> /home/user/opencpi
    
7. Setup the OCPI_LIBRARY_PATH
   $ export OCPI_LIBRARY_PATH=/home/user/opencpi/projects/assets/hdl/assemblies/testbias:
   /home/user/opencpi/projects/core/artifacts:/home/user/opencpi/projects/assets/artifacts/
   	
8. Disable the DMA Cache Mode
    $ export OCPI_DMA_CACHE_MODE=0
  
9. Run the testbias application
    $ ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml

10. Validate the output data by comparing to the input data, when the biasvalue is 0
    $ md5sum test.input
    $ md5sum test.output
\end{verbatim}
\end{appendices}
\end{document}
